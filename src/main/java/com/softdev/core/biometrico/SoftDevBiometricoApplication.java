package com.softdev.core.biometrico;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoftDevBiometricoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoftDevBiometricoApplication.class, args);
	}
}
	