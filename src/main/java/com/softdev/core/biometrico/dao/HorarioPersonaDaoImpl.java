package com.softdev.core.biometrico.dao;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.softdev.core.biometrico.eis.bo.HorarioPersona;
import com.softdev.core.biometrico.eis.bo.Semana;

@Repository
@Transactional
public class HorarioPersonaDaoImpl extends AbstractSession implements HorarioPersonaDAO {

	@Override
	public void saveHorarioPersona(HorarioPersona horarioPersona) {
		getSession().save(horarioPersona);
	}

	@Override
	public void deleteHorarioPersonaById(Long idHorarioPersona) {
		HorarioPersona horarioPersona = findById(idHorarioPersona);
		if(horarioPersona != null) {
			getSession().delete(horarioPersona);
		}
	}

	@Override
	public void updateHorarioPersona(HorarioPersona horarioPersona) {
		getSession().update(horarioPersona);
	}

	@Override
	public List<HorarioPersona> findAllHorarioPersonas() {
		return getSession().createQuery("FROM HorarioPersona").list();
	}

	@Override
	public HorarioPersona findById(Long idHorarioPersona) {
		return getSession().get(HorarioPersona.class, idHorarioPersona);
	}

}
