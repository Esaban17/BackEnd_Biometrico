package com.softdev.core.biometrico.dao;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.AsignacionPersona;
public interface AsignacionPersonaDAO {
	void saveAsignacionPersona(AsignacionPersona asignacionPersona);
	void deleteAsignacionPersona(Long idAsignacion);
	void updateAsignacionPersona(AsignacionPersona asignacionPersona);
	List<AsignacionPersona> findAllAsignacionPersonas();
	AsignacionPersona findById(Long idAsignacion);
	AsignacionPersona findByIdPersona(Long idPersona);
	AsignacionPersona findByIdDepartamento(Long idDepartamento);
	AsignacionPersona findByIdRol(Long idRol);
	AsignacionPersona findByIdNivelAcceso(Long idNivelAcceso);
}
