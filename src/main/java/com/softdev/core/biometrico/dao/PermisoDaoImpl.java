package com.softdev.core.biometrico.dao;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.softdev.core.biometrico.eis.bo.Permiso;

@Repository
@Transactional
public class PermisoDaoImpl extends AbstractSession implements PermisoDAO{

	@Override
	public void savePermiso(Permiso permiso) {
		getSession().save(permiso);
	}

	@Override
	public void deletePermisoById(Long idPermiso) {
		Permiso permiso = findById(idPermiso);
		if(permiso != null) {
			getSession().delete(permiso);
		}
	}

	@Override
	public void updatePermiso(Permiso permiso) {
		getSession().update(permiso);
	}

	@Override
	public List<Permiso> findAllPermisos() {
		return getSession().createQuery("FROM Permiso").list();
	}

	@Override
	public Permiso findById(Long idPermiso) {
		return getSession().get(Permiso.class,idPermiso);
	}

	@Override
	public Permiso findByName(String codigo) {
		return (Permiso)getSession().createQuery("FROM Permiso WHERE codigo = :codigo").setParameter("codigo",codigo).uniqueResult();
		
	}

}
