package com.softdev.core.biometrico.dao;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.softdev.core.biometrico.eis.bo.NivelAcceso;

@Repository
@Transactional
public class NivelAccesoDaoImpl extends AbstractSession implements NivelAccesoDAO {

	@Override
	public void saveNivelAcceso(NivelAcceso nivelAcceso) {
		getSession().save(nivelAcceso);
	}

	@Override
	public void deleteNivelAccesoById(Long idNivelAcceso) {
		NivelAcceso nivelAcceso = findById(idNivelAcceso);
		if(nivelAcceso != null) {
			getSession().delete(nivelAcceso);
		}
	}

	public void deleteNivelAccesoObject(NivelAcceso nivelAcceso) {
		getSession().delete(nivelAcceso);
	}
	
	@Override
	public void updateNivelAcceso(NivelAcceso nivelAcceso) {
		getSession().update(nivelAcceso);
	}

	@Override
	public List<NivelAcceso> findAllNivelesAcceso() {
		return getSession().createQuery("FROM NivelAcceso").list();
	}

	@Override
	public NivelAcceso findById(Long idNivelAcceso) {
		return getSession().get(NivelAcceso.class, idNivelAcceso);
	}

	@Override
	public NivelAcceso findByName(String descripcion) {
		return (NivelAcceso)getSession().createQuery("FROM NivelAcceso WHERE descripcion = :descripcion").setParameter("descripcion", descripcion).uniqueResult();
	}

}
