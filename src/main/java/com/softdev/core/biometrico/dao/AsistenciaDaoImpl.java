package com.softdev.core.biometrico.dao;
import java.sql.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.softdev.core.biometrico.eis.bo.Asistencia;

@Repository
@Transactional
public class AsistenciaDaoImpl extends AbstractSession implements AsistenciaDAO {

	@Override
	public void saveAsistencia(Asistencia asistencia) {
		getSession().save(asistencia);
	}

	@Override
	public void deleteAsistenciaById(Long idAsistencia) {
		Asistencia asistencia = findById(idAsistencia);
		if(asistencia != null) {
			getSession().delete(asistencia);
		}
	}

	@Override
	public void updateAsistencia(Asistencia asistencia) {
		getSession().update(asistencia);
	}

	@Override
	public List<Asistencia> findAllAsistencias() {
		return getSession().createQuery("FROM Asistencia").list();
	}

	@Override
	public Asistencia findById(Long idAsistencia) {
		return getSession().get(Asistencia.class, idAsistencia);
	}

	@Override
	public Asistencia findByDate(Date fecha) {
		return (Asistencia)getSession().createQuery("FROM Asistencia WHERE fecha = :fecha").setParameter("fecha", fecha).uniqueResult();
	}

}
