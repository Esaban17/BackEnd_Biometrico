package com.softdev.core.biometrico.dao;
import java.util.List;

import com.softdev.core.biometrico.eis.bo.NivelAcceso;
import com.softdev.core.biometrico.eis.bo.Permiso;
import com.softdev.core.biometrico.eis.bo.PermisoNivelAcceso;
public interface PermisoNivelAccesoDAO {
	void savePermisoNivelAcceso(PermisoNivelAcceso permisoNivelAcceso);
	void deletePermisoNivelAccesoById(Long idPermisoNivelAcceso);
	void updatePermisoNivelAcceso(PermisoNivelAcceso permisoNivelAcceso);
	List<PermisoNivelAcceso> findAllPermisoNivelAccesos();
	PermisoNivelAcceso findById(Long idPermisoNivelAcceso);
	PermisoNivelAcceso findByIdPermiso(Long idPermiso);
	PermisoNivelAcceso findByIdNivelAcceso(Long idNivelAcceso);
}
