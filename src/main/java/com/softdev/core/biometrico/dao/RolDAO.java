package com.softdev.core.biometrico.dao;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.Rol;
public interface RolDAO {
	void saveRol(Rol rol);
	void deleteRol(Long idRol);
	void updateRol(Rol rol);
	List <Rol> findAllRoles();
	Rol findById(Long idRol);
	Rol findByDescription(String descripcion);
}
