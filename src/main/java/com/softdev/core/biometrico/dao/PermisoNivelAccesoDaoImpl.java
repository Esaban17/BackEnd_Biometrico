package com.softdev.core.biometrico.dao;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;

import com.softdev.core.biometrico.eis.bo.NivelAcceso;
import com.softdev.core.biometrico.eis.bo.Permiso;
import com.softdev.core.biometrico.eis.bo.PermisoNivelAcceso;

@Repository
@Transactional
public class PermisoNivelAccesoDaoImpl extends AbstractSession implements PermisoNivelAccesoDAO {

	@Override
	public void savePermisoNivelAcceso(PermisoNivelAcceso permisoNivelAcceso) {
		getSession().save(permisoNivelAcceso);
	}

	@Override
	public void deletePermisoNivelAccesoById(Long idPermisoNivelAcceso) {
		PermisoNivelAcceso permisoNivelAcceso = findById(idPermisoNivelAcceso);
		if(permisoNivelAcceso != null) {
			getSession().delete(permisoNivelAcceso);
		}
	}

	@Override
	public void updatePermisoNivelAcceso(PermisoNivelAcceso permisoNivelAcceso) {
		getSession().update(permisoNivelAcceso);
	}

	@Override
	public List<PermisoNivelAcceso> findAllPermisoNivelAccesos() {
		return getSession().createQuery("FROM PermisoNivelAcceso").list();
	}

	@Override
	public PermisoNivelAcceso findById(Long idPermisoNivelAcceso) {
		return getSession().get(PermisoNivelAcceso.class , idPermisoNivelAcceso);
	}

	@Override
	public PermisoNivelAcceso findByIdPermiso(Long idPermiso) {
		return getSession().get(PermisoNivelAcceso.class,idPermiso);
	}

	@Override
	public PermisoNivelAcceso findByIdNivelAcceso(Long idNivelAcceso) {
		return getSession().get(PermisoNivelAcceso.class, idNivelAcceso);
	}
}
