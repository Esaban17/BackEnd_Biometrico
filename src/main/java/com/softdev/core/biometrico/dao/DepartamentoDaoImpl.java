package com.softdev.core.biometrico.dao;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.softdev.core.biometrico.eis.bo.Departamento;

@Repository
@Transactional
public class DepartamentoDaoImpl extends AbstractSession implements DepartamentoDAO {

	@Override
	public void saveDepartamento(Departamento departamento) {
		getSession().save(departamento);	
	}
	@Override
	public void deleteDepartamentoById(Long idDepartamento) {
		Departamento departamento = findById(idDepartamento);
		if(departamento != null) {
			getSession().delete(departamento);
		}
	}
	@Override
	public void updateDepartamento(Departamento departamento) {
		getSession().update(departamento);
	}
	@Override
	public List<Departamento> findAllDepartamentos() {
		return getSession().createQuery("FROM Departamento").list();
	}
	@Override
	public Departamento findById(Long idDepartamento) {
		return getSession().get(Departamento.class, idDepartamento);
	}
	@Override
	public Departamento findByName(String nombreDepartamento) {
		
		return (Departamento)getSession().createQuery("FROM Departamento WHERE nombreDepartamento = :nombre")
				.setParameter("nombre", nombreDepartamento).uniqueResult();
	}
}
