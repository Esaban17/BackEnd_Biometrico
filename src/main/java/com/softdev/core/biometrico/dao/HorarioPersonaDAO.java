package com.softdev.core.biometrico.dao;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.HorarioPersona;
import com.softdev.core.biometrico.eis.bo.Semana;
public interface HorarioPersonaDAO {
	void saveHorarioPersona(HorarioPersona horarioPersona);
	void deleteHorarioPersonaById(Long idHorarioPersona);
	void updateHorarioPersona(HorarioPersona horarioPersona);
	List<HorarioPersona> findAllHorarioPersonas();
	HorarioPersona findById(Long idHorarioPersona);
}
