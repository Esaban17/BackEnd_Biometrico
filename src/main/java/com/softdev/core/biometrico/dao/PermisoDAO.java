package com.softdev.core.biometrico.dao;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.Permiso;
public interface PermisoDAO {
	void savePermiso(Permiso permiso);
	void deletePermisoById(Long idPermiso);
	void updatePermiso(Permiso permiso);
	List <Permiso> findAllPermisos();
	Permiso findById(Long idPermiso);
	Permiso findByName(String codigo);
}
