package com.softdev.core.biometrico.dao;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.softdev.core.biometrico.eis.bo.Rol;

@Repository
@Transactional
public class RolDaoImpl extends AbstractSession implements RolDAO {

	@Override
	public void saveRol(Rol rol) {
		getSession().save(rol);
	}

	@Override
	public void deleteRol(Long idRol) {
		Rol rol = findById(idRol);
		if(rol != null) {
			getSession().delete(rol);
		}
	}

	@Override
	public void updateRol(Rol rol) {
		getSession().update(rol);
	}

	@Override
	public List<Rol> findAllRoles() {
		return getSession().createQuery("FROM Rol").list();
	}

	@Override
	public Rol findById(Long idRol) {
		return getSession().get(Rol.class, idRol);
	}

	@Override
	public Rol findByDescription(String descripcion) {
		return (Rol)getSession().createQuery("FROM Rol WHERE descripcion = :descripcion")
				.setParameter("descripcion", descripcion).uniqueResult();
	}

	
}
