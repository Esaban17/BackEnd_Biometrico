package com.softdev.core.biometrico.dao;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.softdev.core.biometrico.eis.bo.Usuario;

@Repository
@Transactional
public class UsuarioDaoImpl extends AbstractSession implements UsuarioDAO{

	@Override
	public void saveUsuario(Usuario usuario) {		
		getSession().save(usuario);
	}

	@Override
	public void deleteUsuarioById(Long idUsuario) {
		Usuario usuario = findById(idUsuario);
		if(usuario != null) {
			getSession().delete(usuario);
		}
	}

	@Override
	public void updateUsuario(Usuario usuario) {
		getSession().update(usuario);
	}

	@Override
	public List<Usuario> findAllUsuarios() {
		return getSession().createQuery("FROM Usuario").list();
	}

	@Override
	public Usuario findById(Long idUsuario) {
		return getSession().get(Usuario.class, idUsuario);
	}

	@Override
	public Usuario findByUsuario(String usuario) {
		return (Usuario)getSession().createQuery("FROM Usuario WHERE usuario = :usuario").setParameter("usuario", usuario).uniqueResult();
	}
}
