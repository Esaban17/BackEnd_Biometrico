package com.softdev.core.biometrico.dao;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.softdev.core.biometrico.dao.AsignacionPersonaDAO;
import com.softdev.core.biometrico.eis.bo.AsignacionPersona;

@Repository
@Transactional
public class AsignacionPersonaDaoImpl extends AbstractSession implements AsignacionPersonaDAO{
	
	
	@Override
	public void saveAsignacionPersona(AsignacionPersona asignacionPersona) {
		getSession().save(asignacionPersona);
	}

	@Override
	public void deleteAsignacionPersona(Long idAsignacion) {
		AsignacionPersona asignacionPersona = findById(idAsignacion);
		if (asignacionPersona != null) {
			getSession().delete(asignacionPersona);
		}
	}

	@Override
	public void updateAsignacionPersona(AsignacionPersona asignacionPersona) {
		getSession().update(asignacionPersona);
	}

	@Override
	public List<AsignacionPersona> findAllAsignacionPersonas() {
		return getSession().createQuery("FROM AsignacionPersona").list();
	}

	@Override
	public AsignacionPersona findById(Long idAsignacion) {
		return (AsignacionPersona)getSession().get(AsignacionPersona.class, idAsignacion);
	}

	@Override
	public AsignacionPersona findByIdPersona(Long idPersona) {
		return (AsignacionPersona)getSession().get(AsignacionPersona.class, idPersona);
	}

	@Override
	public AsignacionPersona findByIdDepartamento(Long idDepartamento) {
		return (AsignacionPersona)getSession().get(AsignacionPersona.class, idDepartamento);
	}

	@Override
	public AsignacionPersona findByIdRol(Long idRol) {
		return (AsignacionPersona)getSession().get(AsignacionPersona.class, idRol);
	}

	@Override
	public AsignacionPersona findByIdNivelAcceso(Long idNivelAcceso) {
		return (AsignacionPersona)getSession().get(AsignacionPersona.class, idNivelAcceso);
	}

}
