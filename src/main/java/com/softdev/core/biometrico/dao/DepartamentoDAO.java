package com.softdev.core.biometrico.dao;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.Departamento;;
public interface DepartamentoDAO {
	void saveDepartamento(Departamento departamento);
	void deleteDepartamentoById(Long idDepartamento);
	void updateDepartamento(Departamento departamento);
	List<Departamento> findAllDepartamentos();
	Departamento findById(Long idDepartamento);
	Departamento findByName(String nombreDepartamento);
}
