package com.softdev.core.biometrico.dao;
import java.sql.Date;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.Asistencia;
public interface AsistenciaDAO {
	void saveAsistencia(Asistencia asistencia);
	void deleteAsistenciaById(Long idAsistencia);
	void updateAsistencia(Asistencia asistencia);
	List<Asistencia> findAllAsistencias();
	Asistencia findById(Long idAsistencia);
	Asistencia findByDate(Date fecha);
}
