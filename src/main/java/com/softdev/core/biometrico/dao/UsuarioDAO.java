package com.softdev.core.biometrico.dao;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.Usuario;
public interface UsuarioDAO {
	void saveUsuario(Usuario usuario);
	void deleteUsuarioById(Long idUsuario);
	void updateUsuario(Usuario usuario);
	List<Usuario> findAllUsuarios();
	Usuario findById(Long idUsuario);
	Usuario findByUsuario(String usuario);
}
