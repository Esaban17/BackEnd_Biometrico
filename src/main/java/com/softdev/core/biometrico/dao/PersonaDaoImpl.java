package com.softdev.core.biometrico.dao;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.stereotype.Repository;
import com.softdev.core.biometrico.eis.bo.Persona;

@Repository
@Transactional
public class PersonaDaoImpl extends AbstractSession implements PersonaDAO {

	@Override
	public void savePersona(Persona persona) {
		getSession().save(persona);
	}

	@Override
	public void deletePersonaById(Long idPersona) {
		Persona persona = findById(idPersona);
		if(persona != null) {
			getSession().delete(persona);
		}
	}

	@Override
	public void updatePersona(Persona persona) {
		getSession().update(persona);
	}

	@Override
	public List<Persona> findAllPersonas() {
		return getSession().createQuery("FROM Persona").list();
	}

	@Override
	public Persona findById(Long idPersona) {
		return getSession().get(Persona.class, idPersona);
	}

	@Override
	public Persona findByCui(Long cui) {
		return (Persona)getSession().createQuery("FROM Persona WHERE cui = :cui").setParameter("cui", cui).uniqueResult();
	}

}
