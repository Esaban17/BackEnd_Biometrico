package com.softdev.core.biometrico.service;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.Usuario;
public interface UsuarioService {
	void saveUsuario(Usuario usuario);
	void deleteUsuarioById(Long idUsuario);
	void updateUsuario(Usuario usuario);
	List<Usuario> findAllUsuarios();
	Usuario findById(Long idUsuario);
	Usuario findByUser(String usuario);
}
