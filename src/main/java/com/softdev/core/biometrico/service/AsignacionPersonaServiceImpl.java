package com.softdev.core.biometrico.service;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.softdev.core.biometrico.eis.bo.AsignacionPersona;
import com.softdev.core.biometrico.dao.AsignacionPersonaDAO;

@Service("asignacionPersonaService")
@Transactional
public class AsignacionPersonaServiceImpl implements AsignacionPersonaService{
	@Autowired
	private AsignacionPersonaDAO asignacionPersonaDao;
	
	@Override
	public void saveAsignacionPersona(AsignacionPersona asignacionPersona) {
		asignacionPersonaDao.saveAsignacionPersona(asignacionPersona);
	}

	@Override
	public void deleteAsignacionPersonaById(Long idAsignacion) {
		asignacionPersonaDao.deleteAsignacionPersona(idAsignacion);
	}

	@Override
	public void updateAsignacionPersona(AsignacionPersona asignacionPersona) {
		asignacionPersonaDao.updateAsignacionPersona(asignacionPersona);
	}

	@Override
	public List<AsignacionPersona> findAllAsignacionPersonas() {
		return asignacionPersonaDao.findAllAsignacionPersonas();
	}

	@Override
	public AsignacionPersona findById(Long idAsignacion) {
		return asignacionPersonaDao.findById(idAsignacion);
	}

	@Override
	public AsignacionPersona findByIdPersona(Long idPersona) {
		return asignacionPersonaDao.findByIdPersona(idPersona);
	}

	@Override
	public AsignacionPersona findByIdDepartamento(Long idDepartamento) {
		return asignacionPersonaDao.findByIdDepartamento(idDepartamento);
	}

	@Override
	public AsignacionPersona findByIdRol(Long idRol) {
		return asignacionPersonaDao.findByIdRol(idRol);
	}

	@Override
	public AsignacionPersona findByIdNivelAcceso(Long idNivelAcceso) {
		return asignacionPersonaDao.findByIdNivelAcceso(idNivelAcceso);
	}

}
