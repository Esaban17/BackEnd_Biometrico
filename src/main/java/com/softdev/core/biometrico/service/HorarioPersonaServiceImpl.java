package com.softdev.core.biometrico.service;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.softdev.core.biometrico.dao.HorarioPersonaDAO;
import com.softdev.core.biometrico.eis.bo.HorarioPersona;

@Service("horarioPersonaService")
@Transactional
public class HorarioPersonaServiceImpl implements HorarioPersonaService{
	@Autowired
	private HorarioPersonaDAO horarioPersonaDao;
	
	@Override
	public void saveHorarioPersona(HorarioPersona horarioPersona) {
		horarioPersonaDao.saveHorarioPersona(horarioPersona);
	}

	@Override
	public void deleteHorarioPersonaById(Long idHorarioPersona) {
		horarioPersonaDao.deleteHorarioPersonaById(idHorarioPersona);
	}

	@Override
	public void updateHorarioPersona(HorarioPersona horarioPersona) {
		horarioPersonaDao.updateHorarioPersona(horarioPersona);
	}

	@Override
	public List<HorarioPersona> findAllHorarioPersonas() {
		return horarioPersonaDao.findAllHorarioPersonas();
	}

	@Override
	public HorarioPersona findById(Long idHorarioPersona) {
		return horarioPersonaDao.findById(idHorarioPersona);
	}

}
