package com.softdev.core.biometrico.service;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.HorarioPersona;
public interface HorarioPersonaService {
	void saveHorarioPersona(HorarioPersona horarioPersona);
	void deleteHorarioPersonaById(Long idHorarioPersona);
	void updateHorarioPersona(HorarioPersona horarioPersona);
	List<HorarioPersona> findAllHorarioPersonas();
	HorarioPersona findById(Long idHorarioPersona);
}
