package com.softdev.core.biometrico.service;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.softdev.core.biometrico.dao.RolDAO;
import com.softdev.core.biometrico.eis.bo.Rol;

@Service("rolService")
@Transactional
public class RolServiceImpl implements RolService {
	@Autowired
	private RolDAO rolDao;

	@Override
	public void saveRol(Rol rol) {
		rolDao.saveRol(rol);
	}

	@Override
	public void deleteRolById(Long idRol) {
		rolDao.deleteRol(idRol);
	}

	@Override
	public void updateRol(Rol rol) {	
		rolDao.updateRol(rol);
	}

	@Override
	public List<Rol> findAllRoles() {
		return rolDao.findAllRoles();
	}

	@Override
	public Rol findById(Long idRol) {
		return rolDao.findById(idRol);
	}

	@Override
	public Rol findByDescription(String descripcion) {
		return rolDao.findByDescription(descripcion);
	}
	
	
}
