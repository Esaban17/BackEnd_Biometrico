package com.softdev.core.biometrico.service;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.softdev.core.biometrico.dao.PermisoDAO;
import com.softdev.core.biometrico.eis.bo.Permiso;

@Service("permisoService")
@Transactional
public class PermisoServiceImpl implements PermisoService {
	@Autowired
	private PermisoDAO permisoDao;
	
	@Override
	public void savePermiso(Permiso permiso) {
		permisoDao.savePermiso(permiso);
	}

	@Override
	public void deletePermisoById(Long idPermiso) {
		permisoDao.deletePermisoById(idPermiso);
	}

	@Override
	public void updatePermiso(Permiso permiso) {
		permisoDao.updatePermiso(permiso);
	}

	@Override
	public List<Permiso> findAllPermisos() {
		return permisoDao.findAllPermisos();
	}

	@Override
	public Permiso findById(Long idPermiso) {
		return permisoDao.findById(idPermiso);
	}

	@Override
	public Permiso findByName(String descripcion) {
		return permisoDao.findByName(descripcion);
	}

}
