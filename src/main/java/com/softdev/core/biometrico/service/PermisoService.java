package com.softdev.core.biometrico.service;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.Permiso;
public interface PermisoService {
	void savePermiso(Permiso permiso);
	void deletePermisoById(Long idPermiso);
	void updatePermiso(Permiso permiso);
	List <Permiso> findAllPermisos();
	Permiso findById(Long idPermiso);
	Permiso findByName(String descripcion);
}
