package com.softdev.core.biometrico.service;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.Persona;
public interface PersonaService {
	void savePersona(Persona persona);
	void deletePersonaById(Long idPersona);
	void updatePersona(Persona persona);
	List<Persona> findAllPersonas();
	Persona findById(Long idPersona);
	Persona findByCui(Long cui);
}
