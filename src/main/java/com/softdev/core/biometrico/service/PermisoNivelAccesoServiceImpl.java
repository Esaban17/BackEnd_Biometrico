package com.softdev.core.biometrico.service;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.softdev.core.biometrico.dao.PermisoNivelAccesoDAO;
import com.softdev.core.biometrico.eis.bo.PermisoNivelAcceso;

@Service("permisoNivelAccesoService")
@Transactional
public class PermisoNivelAccesoServiceImpl implements PermisoNivelAccesoService{
	@Autowired
	private PermisoNivelAccesoDAO permisoNivelAccesoDao;
	
	@Override
	public void savePermisoNivelAcceso(PermisoNivelAcceso permisoNivelAcceso) {
		permisoNivelAccesoDao.savePermisoNivelAcceso(permisoNivelAcceso);
	}

	@Override
	public void deletePermisoNivelAccesoById(Long idPermisoNivelAcceso) {
		permisoNivelAccesoDao.deletePermisoNivelAccesoById(idPermisoNivelAcceso);
	}

	@Override
	public void updatePermisoNivelAcceso(PermisoNivelAcceso permisoNivelAcceso) {
		permisoNivelAccesoDao.updatePermisoNivelAcceso(permisoNivelAcceso);
	}

	@Override
	public List<PermisoNivelAcceso> findAllPermisoNivelAccesos() {
		return permisoNivelAccesoDao.findAllPermisoNivelAccesos();
	}

	@Override
	public PermisoNivelAcceso findById(Long idPermisoNivelAcceso) {
		return permisoNivelAccesoDao.findById(idPermisoNivelAcceso);
	}

	@Override
	public PermisoNivelAcceso findByIdPermiso(Long idPermiso) {
		return permisoNivelAccesoDao.findByIdPermiso(idPermiso);
	}

	@Override
	public PermisoNivelAcceso findByIdNivelAcceso(Long idNivelAcceso) {
		return permisoNivelAccesoDao.findByIdNivelAcceso(idNivelAcceso);
	}

}
