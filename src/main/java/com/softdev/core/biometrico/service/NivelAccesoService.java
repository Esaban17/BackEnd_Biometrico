package com.softdev.core.biometrico.service;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.NivelAcceso;
public interface NivelAccesoService {
	void saveNivelAcceso(NivelAcceso nivelAcceso);
	void deleteNivelAccesoById(Long idNivelAcceso);
	void updateNivelAcceso(NivelAcceso nivelAcceso);
	List <NivelAcceso> findAllNivelesAcceso();
	NivelAcceso findById(Long idNivelAcceso);
	NivelAcceso findByName(String descripcion);
}
