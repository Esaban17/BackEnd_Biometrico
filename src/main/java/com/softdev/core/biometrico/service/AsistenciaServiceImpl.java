package com.softdev.core.biometrico.service;
import java.sql.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.softdev.core.biometrico.dao.AsistenciaDAO;
import com.softdev.core.biometrico.eis.bo.Asistencia;

@Service("asistenciaService")
@Transactional
public class AsistenciaServiceImpl implements AsistenciaService {
	@Autowired
	private AsistenciaDAO asistenciaDao;
	
	@Override
	public void saveAsistencia(Asistencia asistencia) {
		asistenciaDao.saveAsistencia(asistencia);
	}

	@Override
	public void deleteAsistenciaById(Long idAsistencia) {
		asistenciaDao.deleteAsistenciaById(idAsistencia);
	}

	@Override
	public void updateAsistencia(Asistencia asistencia) {
		asistenciaDao.updateAsistencia(asistencia);
	}

	@Override
	public List<Asistencia> findAllAsistencias() {
		return asistenciaDao.findAllAsistencias();
	}

	@Override
	public Asistencia findById(Long idAsistencia) {
		return asistenciaDao.findById(idAsistencia);
	}

	@Override
	public Asistencia findByDate(Date fecha) {
		return asistenciaDao.findByDate(fecha);
	}

}
