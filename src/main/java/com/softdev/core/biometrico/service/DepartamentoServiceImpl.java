package com.softdev.core.biometrico.service;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.softdev.core.biometrico.dao.DepartamentoDAO;
import com.softdev.core.biometrico.eis.bo.Departamento;

@Service("departamentoService")
@Transactional
public class DepartamentoServiceImpl implements DepartamentoService {
	@Autowired
	private DepartamentoDAO departamentoDao;
	
	@Override
	public void saveDepartamento(Departamento departamento) {
		departamentoDao.saveDepartamento(departamento);
	}

	@Override
	public void deleteDepartamentoById(Long idDepartamento) {
		departamentoDao.deleteDepartamentoById(idDepartamento);
	}

	@Override
	public void updateDepartamento(Departamento departamento) {
		departamentoDao.updateDepartamento(departamento);
	}

	@Override
	public List<Departamento> findAllDepartamentos() {
		return departamentoDao.findAllDepartamentos();
	}

	@Override
	public Departamento findById(Long idDepartamento) {
		return departamentoDao.findById(idDepartamento);
	}

	@Override
	public Departamento findByName(String nombreDepartamento) {
		return departamentoDao.findByName(nombreDepartamento);
	}

}
