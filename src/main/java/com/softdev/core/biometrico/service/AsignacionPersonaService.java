package com.softdev.core.biometrico.service;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.AsignacionPersona;
public interface AsignacionPersonaService {
	void saveAsignacionPersona(AsignacionPersona asignacionPersona);
	void deleteAsignacionPersonaById(Long idAsignacion);
	void updateAsignacionPersona(AsignacionPersona asignacionPersona);
	List<AsignacionPersona> findAllAsignacionPersonas();
	AsignacionPersona findById(Long idAsignacion);
	AsignacionPersona findByIdPersona(Long idPersona);
	AsignacionPersona findByIdDepartamento(Long idDepartamento);
	AsignacionPersona findByIdRol(Long idRol);
	AsignacionPersona findByIdNivelAcceso(Long idNivelAcceso);
}
