package com.softdev.core.biometrico.service;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.softdev.core.biometrico.dao.PersonaDAO;
import com.softdev.core.biometrico.eis.bo.Persona;

@Service("personaService")
@Transactional
public class PersonaServiceImpl implements PersonaService {
	@Autowired
	private PersonaDAO personaDao;
	
	@Override
	public void savePersona(Persona persona) {
		personaDao.savePersona(persona);
	}

	@Override
	public void deletePersonaById(Long idPersona) {
		personaDao.deletePersonaById(idPersona);
	}

	@Override
	public void updatePersona(Persona persona) {
		personaDao.updatePersona(persona);
	}

	@Override
	public List<Persona> findAllPersonas() {
		return personaDao.findAllPersonas();
	}

	@Override
	public Persona findById(Long idPersona) {
		return personaDao.findById(idPersona);
	}

	@Override
	public Persona findByCui(Long cui) {
		return personaDao.findByCui(cui);
	}

}
