package com.softdev.core.biometrico.service;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.softdev.core.biometrico.dao.NivelAccesoDAO;
import com.softdev.core.biometrico.eis.bo.NivelAcceso;

@Service("nivelAccesoService")
@Transactional
public class NivelAccesoServiceImpl implements NivelAccesoService {
	@Autowired
	private NivelAccesoDAO nivelAccesoDao;
	
	@Override
	public void saveNivelAcceso(NivelAcceso nivelAcceso) {
		nivelAccesoDao.saveNivelAcceso(nivelAcceso);
	}

	@Override
	public void deleteNivelAccesoById(Long idNivelAcceso) {
		nivelAccesoDao.deleteNivelAccesoById(idNivelAcceso);
	}

	@Override
	public void updateNivelAcceso(NivelAcceso nivelAcceso) {
		nivelAccesoDao.updateNivelAcceso(nivelAcceso);
	}

	@Override
	public List<NivelAcceso> findAllNivelesAcceso() {
		return nivelAccesoDao.findAllNivelesAcceso();
	}

	@Override
	public NivelAcceso findById(Long idNivelAcceso) {
		return nivelAccesoDao.findById(idNivelAcceso);
	}

	@Override
	public NivelAcceso findByName(String descripcion) {
		return nivelAccesoDao.findByName(descripcion);
	}

}
