package com.softdev.core.biometrico.service;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.PermisoNivelAcceso;
public interface PermisoNivelAccesoService{
	void savePermisoNivelAcceso(PermisoNivelAcceso permisoNivelAcceso);
	void deletePermisoNivelAccesoById(Long idPermisoNivelAcceso);
	void updatePermisoNivelAcceso(PermisoNivelAcceso permisoNivelAcceso);
	List<PermisoNivelAcceso> findAllPermisoNivelAccesos();
	PermisoNivelAcceso findById(Long idPermisoNivelAcceso);
	PermisoNivelAcceso findByIdPermiso(Long idPermiso);
	PermisoNivelAcceso findByIdNivelAcceso(Long idNivelAcceso);
}
