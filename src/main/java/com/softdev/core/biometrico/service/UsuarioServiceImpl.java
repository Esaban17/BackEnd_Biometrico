package com.softdev.core.biometrico.service;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.softdev.core.biometrico.dao.UsuarioDAO;
import com.softdev.core.biometrico.eis.bo.Usuario;

@Service("usuarioService")
@Transactional
public class UsuarioServiceImpl implements UsuarioService{
	@Autowired
	private UsuarioDAO usuarioDao;
	
	@Override
	public void saveUsuario(Usuario usuario) {
		usuarioDao.saveUsuario(usuario);
	}

	@Override
	public void deleteUsuarioById(Long idUsuario) {
		usuarioDao.deleteUsuarioById(idUsuario);
	}

	@Override
	public void updateUsuario(Usuario usuario) {
		usuarioDao.updateUsuario(usuario);
	}

	@Override
	public List<Usuario> findAllUsuarios() {
		return usuarioDao.findAllUsuarios();
	}

	@Override
	public Usuario findById(Long idUsuario) {
		return usuarioDao.findById(idUsuario);
	}

	@Override
	public Usuario findByUser(String usuario) {
		return usuarioDao.findByUsuario(usuario);
	}

}
