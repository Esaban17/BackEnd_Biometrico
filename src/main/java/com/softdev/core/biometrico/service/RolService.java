package com.softdev.core.biometrico.service;
import java.util.List;
import com.softdev.core.biometrico.eis.bo.Rol;
public interface RolService {
	void saveRol(Rol rol);
	void deleteRolById(Long idRol);
	void updateRol(Rol rol);
	List<Rol> findAllRoles();
	Rol findById(Long idRol);
	Rol findByDescription(String descripcion);
}
