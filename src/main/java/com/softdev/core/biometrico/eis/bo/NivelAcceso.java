package com.softdev.core.biometrico.eis.bo;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
@Table(name="nivelAcceso",schema ="biometrico")
public class NivelAcceso  implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "idNivelAcceso")
	private Long idNivelAcceso;
	@Column (name = "descripcion", nullable = false)
	private String descripcion;
	@Column (name = "codigo", nullable = false)
	private String codigo;
	
	@OneToMany(mappedBy = "nivelAcceso",fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<AsignacionPersona> listaAsignacionPersonas;
	
	@OneToMany(mappedBy = "nivelAcceso",fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<PermisoNivelAcceso> listaPermisoNivelAccesos;
	
	public NivelAcceso() {
		
	}

	public Long getIdNivelAcceso() {
		return idNivelAcceso;
	}

	public void setIdNivelAcceso(Long idNivelAcceso) {
		this.idNivelAcceso = idNivelAcceso;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Set<AsignacionPersona> getListaAsignacionPersonas() {
		return listaAsignacionPersonas;
	}

	public void setListaAsignacionPersonas(Set<AsignacionPersona> listaAsignacionPersonas) {
		this.listaAsignacionPersonas = listaAsignacionPersonas;
	}

	public Set<PermisoNivelAcceso> getListaPermisoNivelAccesos() {
		return listaPermisoNivelAccesos;
	}

	public void setListaPermisoNivelAccesos(Set<PermisoNivelAcceso> listaPermisoNivelAccesos) {
		this.listaPermisoNivelAccesos = listaPermisoNivelAccesos;
	}
	
}
