package com.softdev.core.biometrico.eis.dto;
import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.softdev.core.biometrico.eis.bo.Persona;
public class PersonaDTO implements Serializable{
	private Long idPersona;
	private int activo;
	private Long cui;
	private String primerNombre;
	private String segundoNombre;
	private String tercerNombre;
	private String primerApellido;
	private String segundoApellido;
	private String tercerApellido;
	private String genero;
	private String nacionalidadMunicipio;
	private String nacionalidadDepartamento;
	private String nacionalidadPais;
	private Date fechaNacimiento;
	private Date fechaEmisionDocumento;
	private Date fechaVencimientoDocumento;
	private String estadoCivil;
	private String vecindadMunicipio;
	private String vecindadDepartamento;
	private String nacionalidad;
	private String sabeLeer;
	private String sabeEscribir;
	private String limitacionesFisicas;
	private String libro;
	private String folio;
	private String partida;
	private String residenciaDireccion;
	private String residenciaMunicipio;
	private String residenciaDepartamento;
	private String codigoPostal;
	private String residenciaPais;
	private String profesion;
	private String cedulaNumero;
	private String celudaMunicipio;
	private String cedulaDepartamento;
	private String oficialActivo;
	private byte fotografia;
	private byte huellaDactilar;
	private String MRZ;
	private String numeroSerieDocumento;
	private String correoPersonal;
	private String correoInstitucional;
	private String numeroTelefonico;
	private String numeroTelefonicoMovil;
	
	
	public PersonaDTO() {
		
	}


	public PersonaDTO(Long idPersona, int activo, Long cui, String primerNombre, String segundoNombre,
			String tercerNombre, String primerApellido, String segundoApellido, String tercerApellido, String genero,
			String nacionalidadMunicipio, String nacionalidadDepartamento, String nacionalidadPais,
			Date fechaNacimiento, Date fechaEmisionDocumento, Date fechaVencimientoDocumento, String estadoCivil,
			String vecindadMunicipio, String vecindadDepartamento, String nacionalidad, String sabeLeer,
			String sabeEscribir, String limitacionesFisicas, String libro, String folio, String partida,
			String residenciaDireccion, String residenciaMunicipio, String residenciaDepartamento, String codigoPostal,
			String residenciaPais, String profesion, String cedulaNumero, String celudaMunicipio,
			String cedulaDepartamento, String oficialActivo, byte fotografia, byte huellaDactilar, String mRZ,
			String numeroSerieDocumento, String correoPersonal, String correoInstitucional, String numeroTelefonico,
			String numeroTelefonicoMovil) {
		super();
		this.idPersona = idPersona;
		this.activo = activo;
		this.cui = cui;
		this.primerNombre = primerNombre;
		this.segundoNombre = segundoNombre;
		this.tercerNombre = tercerNombre;
		this.primerApellido = primerApellido;
		this.segundoApellido = segundoApellido;
		this.tercerApellido = tercerApellido;
		this.genero = genero;
		this.nacionalidadMunicipio = nacionalidadMunicipio;
		this.nacionalidadDepartamento = nacionalidadDepartamento;
		this.nacionalidadPais = nacionalidadPais;
		this.fechaNacimiento = fechaNacimiento;
		this.fechaEmisionDocumento = fechaEmisionDocumento;
		this.fechaVencimientoDocumento = fechaVencimientoDocumento;
		this.estadoCivil = estadoCivil;
		this.vecindadMunicipio = vecindadMunicipio;
		this.vecindadDepartamento = vecindadDepartamento;
		this.nacionalidad = nacionalidad;
		this.sabeLeer = sabeLeer;
		this.sabeEscribir = sabeEscribir;
		this.limitacionesFisicas = limitacionesFisicas;
		this.libro = libro;
		this.folio = folio;
		this.partida = partida;
		this.residenciaDireccion = residenciaDireccion;
		this.residenciaMunicipio = residenciaMunicipio;
		this.residenciaDepartamento = residenciaDepartamento;
		this.codigoPostal = codigoPostal;
		this.residenciaPais = residenciaPais;
		this.profesion = profesion;
		this.cedulaNumero = cedulaNumero;
		this.celudaMunicipio = celudaMunicipio;
		this.cedulaDepartamento = cedulaDepartamento;
		this.oficialActivo = oficialActivo;
		this.fotografia = fotografia;
		this.huellaDactilar = huellaDactilar;
		MRZ = mRZ;
		this.numeroSerieDocumento = numeroSerieDocumento;
		this.correoPersonal = correoPersonal;
		this.correoInstitucional = correoInstitucional;
		this.numeroTelefonico = numeroTelefonico;
		this.numeroTelefonicoMovil = numeroTelefonicoMovil;
	}


	public Long getIdPersona() {
		return idPersona;
	}


	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}


	public int getActivo() {
		return activo;
	}


	public void setActivo(int activo) {
		this.activo = activo;
	}


	public Long getCui() {
		return cui;
	}


	public void setCui(Long cui) {
		this.cui = cui;
	}


	public String getPrimerNombre() {
		return primerNombre;
	}


	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}


	public String getSegundoNombre() {
		return segundoNombre;
	}


	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}


	public String getTercerNombre() {
		return tercerNombre;
	}


	public void setTercerNombre(String tercerNombre) {
		this.tercerNombre = tercerNombre;
	}


	public String getPrimerApellido() {
		return primerApellido;
	}


	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}


	public String getSegundoApellido() {
		return segundoApellido;
	}


	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}


	public String getTercerApellido() {
		return tercerApellido;
	}


	public void setTercerApellido(String tercerApellido) {
		this.tercerApellido = tercerApellido;
	}


	public String getGenero() {
		return genero;
	}


	public void setGenero(String genero) {
		this.genero = genero;
	}


	public String getNacionalidadMunicipio() {
		return nacionalidadMunicipio;
	}


	public void setNacionalidadMunicipio(String nacionalidadMunicipio) {
		this.nacionalidadMunicipio = nacionalidadMunicipio;
	}


	public String getNacionalidadDepartamento() {
		return nacionalidadDepartamento;
	}


	public void setNacionalidadDepartamento(String nacionalidadDepartamento) {
		this.nacionalidadDepartamento = nacionalidadDepartamento;
	}


	public String getNacionalidadPais() {
		return nacionalidadPais;
	}


	public void setNacionalidadPais(String nacionalidadPais) {
		this.nacionalidadPais = nacionalidadPais;
	}


	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}


	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}


	public Date getFechaEmisionDocumento() {
		return fechaEmisionDocumento;
	}


	public void setFechaEmisionDocumento(Date fechaEmisionDocumento) {
		this.fechaEmisionDocumento = fechaEmisionDocumento;
	}


	public Date getFechaVencimientoDocumento() {
		return fechaVencimientoDocumento;
	}


	public void setFechaVencimientoDocumento(Date fechaVencimientoDocumento) {
		this.fechaVencimientoDocumento = fechaVencimientoDocumento;
	}


	public String getEstadoCivil() {
		return estadoCivil;
	}


	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}


	public String getVecindadMunicipio() {
		return vecindadMunicipio;
	}


	public void setVecindadMunicipio(String vecindadMunicipio) {
		this.vecindadMunicipio = vecindadMunicipio;
	}


	public String getVecindadDepartamento() {
		return vecindadDepartamento;
	}


	public void setVecindadDepartamento(String vecindadDepartamento) {
		this.vecindadDepartamento = vecindadDepartamento;
	}


	public String getNacionalidad() {
		return nacionalidad;
	}


	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}


	public String getSabeLeer() {
		return sabeLeer;
	}


	public void setSabeLeer(String sabeLeer) {
		this.sabeLeer = sabeLeer;
	}


	public String getSabeEscribir() {
		return sabeEscribir;
	}


	public void setSabeEscribir(String sabeEscribir) {
		this.sabeEscribir = sabeEscribir;
	}


	public String getLimitacionesFisicas() {
		return limitacionesFisicas;
	}


	public void setLimitacionesFisicas(String limitacionesFisicas) {
		this.limitacionesFisicas = limitacionesFisicas;
	}


	public String getLibro() {
		return libro;
	}


	public void setLibro(String libro) {
		this.libro = libro;
	}


	public String getFolio() {
		return folio;
	}


	public void setFolio(String folio) {
		this.folio = folio;
	}


	public String getPartida() {
		return partida;
	}


	public void setPartida(String partida) {
		this.partida = partida;
	}


	public String getResidenciaDireccion() {
		return residenciaDireccion;
	}


	public void setResidenciaDireccion(String residenciaDireccion) {
		this.residenciaDireccion = residenciaDireccion;
	}


	public String getResidenciaMunicipio() {
		return residenciaMunicipio;
	}


	public void setResidenciaMunicipio(String residenciaMunicipio) {
		this.residenciaMunicipio = residenciaMunicipio;
	}


	public String getResidenciaDepartamento() {
		return residenciaDepartamento;
	}


	public void setResidenciaDepartamento(String residenciaDepartamento) {
		this.residenciaDepartamento = residenciaDepartamento;
	}


	public String getCodigoPostal() {
		return codigoPostal;
	}


	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}


	public String getResidenciaPais() {
		return residenciaPais;
	}


	public void setResidenciaPais(String residenciaPais) {
		this.residenciaPais = residenciaPais;
	}


	public String getProfesion() {
		return profesion;
	}


	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}


	public String getCedulaNumero() {
		return cedulaNumero;
	}


	public void setCedulaNumero(String cedulaNumero) {
		this.cedulaNumero = cedulaNumero;
	}


	public String getCeludaMunicipio() {
		return celudaMunicipio;
	}


	public void setCeludaMunicipio(String celudaMunicipio) {
		this.celudaMunicipio = celudaMunicipio;
	}


	public String getCedulaDepartamento() {
		return cedulaDepartamento;
	}


	public void setCedulaDepartamento(String cedulaDepartamento) {
		this.cedulaDepartamento = cedulaDepartamento;
	}


	public String getOficialActivo() {
		return oficialActivo;
	}


	public void setOficialActivo(String oficialActivo) {
		this.oficialActivo = oficialActivo;
	}


	public byte getFotografia() {
		return fotografia;
	}


	public void setFotografia(byte fotografia) {
		this.fotografia = fotografia;
	}


	public byte getHuellaDactilar() {
		return huellaDactilar;
	}


	public void setHuellaDactilar(byte huellaDactilar) {
		this.huellaDactilar = huellaDactilar;
	}


	public String getMRZ() {
		return MRZ;
	}


	public void setMRZ(String mRZ) {
		MRZ = mRZ;
	}


	public String getNumeroSerieDocumento() {
		return numeroSerieDocumento;
	}


	public void setNumeroSerieDocumento(String numeroSerieDocumento) {
		this.numeroSerieDocumento = numeroSerieDocumento;
	}


	public String getCorreoPersonal() {
		return correoPersonal;
	}


	public void setCorreoPersonal(String correoPersonal) {
		this.correoPersonal = correoPersonal;
	}


	public String getCorreoInstitucional() {
		return correoInstitucional;
	}


	public void setCorreoInstitucional(String correoInstitucional) {
		this.correoInstitucional = correoInstitucional;
	}


	public String getNumeroTelefonico() {
		return numeroTelefonico;
	}


	public void setNumeroTelefonico(String numeroTelefonico) {
		this.numeroTelefonico = numeroTelefonico;
	}


	public String getNumeroTelefonicoMovil() {
		return numeroTelefonicoMovil;
	}


	public void setNumeroTelefonicoMovil(String numeroTelefonicoMovil) {
		this.numeroTelefonicoMovil = numeroTelefonicoMovil;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((MRZ == null) ? 0 : MRZ.hashCode());
		result = prime * result + activo;
		result = prime * result + ((cedulaDepartamento == null) ? 0 : cedulaDepartamento.hashCode());
		result = prime * result + ((cedulaNumero == null) ? 0 : cedulaNumero.hashCode());
		result = prime * result + ((celudaMunicipio == null) ? 0 : celudaMunicipio.hashCode());
		result = prime * result + ((codigoPostal == null) ? 0 : codigoPostal.hashCode());
		result = prime * result + ((correoInstitucional == null) ? 0 : correoInstitucional.hashCode());
		result = prime * result + ((correoPersonal == null) ? 0 : correoPersonal.hashCode());
		result = prime * result + ((cui == null) ? 0 : cui.hashCode());
		result = prime * result + ((estadoCivil == null) ? 0 : estadoCivil.hashCode());
		result = prime * result + ((fechaEmisionDocumento == null) ? 0 : fechaEmisionDocumento.hashCode());
		result = prime * result + ((fechaNacimiento == null) ? 0 : fechaNacimiento.hashCode());
		result = prime * result + ((fechaVencimientoDocumento == null) ? 0 : fechaVencimientoDocumento.hashCode());
		result = prime * result + ((folio == null) ? 0 : folio.hashCode());
		result = prime * result + fotografia;
		result = prime * result + ((genero == null) ? 0 : genero.hashCode());
		result = prime * result + huellaDactilar;
		result = prime * result + ((idPersona == null) ? 0 : idPersona.hashCode());
		result = prime * result + ((libro == null) ? 0 : libro.hashCode());
		result = prime * result + ((limitacionesFisicas == null) ? 0 : limitacionesFisicas.hashCode());
		result = prime * result + ((nacionalidad == null) ? 0 : nacionalidad.hashCode());
		result = prime * result + ((nacionalidadDepartamento == null) ? 0 : nacionalidadDepartamento.hashCode());
		result = prime * result + ((nacionalidadMunicipio == null) ? 0 : nacionalidadMunicipio.hashCode());
		result = prime * result + ((nacionalidadPais == null) ? 0 : nacionalidadPais.hashCode());
		result = prime * result + ((numeroSerieDocumento == null) ? 0 : numeroSerieDocumento.hashCode());
		result = prime * result + ((numeroTelefonico == null) ? 0 : numeroTelefonico.hashCode());
		result = prime * result + ((numeroTelefonicoMovil == null) ? 0 : numeroTelefonicoMovil.hashCode());
		result = prime * result + ((oficialActivo == null) ? 0 : oficialActivo.hashCode());
		result = prime * result + ((partida == null) ? 0 : partida.hashCode());
		result = prime * result + ((primerApellido == null) ? 0 : primerApellido.hashCode());
		result = prime * result + ((primerNombre == null) ? 0 : primerNombre.hashCode());
		result = prime * result + ((profesion == null) ? 0 : profesion.hashCode());
		result = prime * result + ((residenciaDepartamento == null) ? 0 : residenciaDepartamento.hashCode());
		result = prime * result + ((residenciaDireccion == null) ? 0 : residenciaDireccion.hashCode());
		result = prime * result + ((residenciaMunicipio == null) ? 0 : residenciaMunicipio.hashCode());
		result = prime * result + ((residenciaPais == null) ? 0 : residenciaPais.hashCode());
		result = prime * result + ((sabeEscribir == null) ? 0 : sabeEscribir.hashCode());
		result = prime * result + ((sabeLeer == null) ? 0 : sabeLeer.hashCode());
		result = prime * result + ((segundoApellido == null) ? 0 : segundoApellido.hashCode());
		result = prime * result + ((segundoNombre == null) ? 0 : segundoNombre.hashCode());
		result = prime * result + ((tercerApellido == null) ? 0 : tercerApellido.hashCode());
		result = prime * result + ((tercerNombre == null) ? 0 : tercerNombre.hashCode());
		result = prime * result + ((vecindadDepartamento == null) ? 0 : vecindadDepartamento.hashCode());
		result = prime * result + ((vecindadMunicipio == null) ? 0 : vecindadMunicipio.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PersonaDTO other = (PersonaDTO) obj;
		if (MRZ == null) {
			if (other.MRZ != null)
				return false;
		} else if (!MRZ.equals(other.MRZ))
			return false;
		if (activo != other.activo)
			return false;
		if (cedulaDepartamento == null) {
			if (other.cedulaDepartamento != null)
				return false;
		} else if (!cedulaDepartamento.equals(other.cedulaDepartamento))
			return false;
		if (cedulaNumero == null) {
			if (other.cedulaNumero != null)
				return false;
		} else if (!cedulaNumero.equals(other.cedulaNumero))
			return false;
		if (celudaMunicipio == null) {
			if (other.celudaMunicipio != null)
				return false;
		} else if (!celudaMunicipio.equals(other.celudaMunicipio))
			return false;
		if (codigoPostal == null) {
			if (other.codigoPostal != null)
				return false;
		} else if (!codigoPostal.equals(other.codigoPostal))
			return false;
		if (correoInstitucional == null) {
			if (other.correoInstitucional != null)
				return false;
		} else if (!correoInstitucional.equals(other.correoInstitucional))
			return false;
		if (correoPersonal == null) {
			if (other.correoPersonal != null)
				return false;
		} else if (!correoPersonal.equals(other.correoPersonal))
			return false;
		if (cui == null) {
			if (other.cui != null)
				return false;
		} else if (!cui.equals(other.cui))
			return false;
		if (estadoCivil == null) {
			if (other.estadoCivil != null)
				return false;
		} else if (!estadoCivil.equals(other.estadoCivil))
			return false;
		if (fechaEmisionDocumento == null) {
			if (other.fechaEmisionDocumento != null)
				return false;
		} else if (!fechaEmisionDocumento.equals(other.fechaEmisionDocumento))
			return false;
		if (fechaNacimiento == null) {
			if (other.fechaNacimiento != null)
				return false;
		} else if (!fechaNacimiento.equals(other.fechaNacimiento))
			return false;
		if (fechaVencimientoDocumento == null) {
			if (other.fechaVencimientoDocumento != null)
				return false;
		} else if (!fechaVencimientoDocumento.equals(other.fechaVencimientoDocumento))
			return false;
		if (folio == null) {
			if (other.folio != null)
				return false;
		} else if (!folio.equals(other.folio))
			return false;
		if (fotografia != other.fotografia)
			return false;
		if (genero == null) {
			if (other.genero != null)
				return false;
		} else if (!genero.equals(other.genero))
			return false;
		if (huellaDactilar != other.huellaDactilar)
			return false;
		if (idPersona == null) {
			if (other.idPersona != null)
				return false;
		} else if (!idPersona.equals(other.idPersona))
			return false;
		if (libro == null) {
			if (other.libro != null)
				return false;
		} else if (!libro.equals(other.libro))
			return false;
		if (limitacionesFisicas == null) {
			if (other.limitacionesFisicas != null)
				return false;
		} else if (!limitacionesFisicas.equals(other.limitacionesFisicas))
			return false;
		if (nacionalidad == null) {
			if (other.nacionalidad != null)
				return false;
		} else if (!nacionalidad.equals(other.nacionalidad))
			return false;
		if (nacionalidadDepartamento == null) {
			if (other.nacionalidadDepartamento != null)
				return false;
		} else if (!nacionalidadDepartamento.equals(other.nacionalidadDepartamento))
			return false;
		if (nacionalidadMunicipio == null) {
			if (other.nacionalidadMunicipio != null)
				return false;
		} else if (!nacionalidadMunicipio.equals(other.nacionalidadMunicipio))
			return false;
		if (nacionalidadPais == null) {
			if (other.nacionalidadPais != null)
				return false;
		} else if (!nacionalidadPais.equals(other.nacionalidadPais))
			return false;
		if (numeroSerieDocumento == null) {
			if (other.numeroSerieDocumento != null)
				return false;
		} else if (!numeroSerieDocumento.equals(other.numeroSerieDocumento))
			return false;
		if (numeroTelefonico == null) {
			if (other.numeroTelefonico != null)
				return false;
		} else if (!numeroTelefonico.equals(other.numeroTelefonico))
			return false;
		if (numeroTelefonicoMovil == null) {
			if (other.numeroTelefonicoMovil != null)
				return false;
		} else if (!numeroTelefonicoMovil.equals(other.numeroTelefonicoMovil))
			return false;
		if (oficialActivo == null) {
			if (other.oficialActivo != null)
				return false;
		} else if (!oficialActivo.equals(other.oficialActivo))
			return false;
		if (partida == null) {
			if (other.partida != null)
				return false;
		} else if (!partida.equals(other.partida))
			return false;
		if (primerApellido == null) {
			if (other.primerApellido != null)
				return false;
		} else if (!primerApellido.equals(other.primerApellido))
			return false;
		if (primerNombre == null) {
			if (other.primerNombre != null)
				return false;
		} else if (!primerNombre.equals(other.primerNombre))
			return false;
		if (profesion == null) {
			if (other.profesion != null)
				return false;
		} else if (!profesion.equals(other.profesion))
			return false;
		if (residenciaDepartamento == null) {
			if (other.residenciaDepartamento != null)
				return false;
		} else if (!residenciaDepartamento.equals(other.residenciaDepartamento))
			return false;
		if (residenciaDireccion == null) {
			if (other.residenciaDireccion != null)
				return false;
		} else if (!residenciaDireccion.equals(other.residenciaDireccion))
			return false;
		if (residenciaMunicipio == null) {
			if (other.residenciaMunicipio != null)
				return false;
		} else if (!residenciaMunicipio.equals(other.residenciaMunicipio))
			return false;
		if (residenciaPais == null) {
			if (other.residenciaPais != null)
				return false;
		} else if (!residenciaPais.equals(other.residenciaPais))
			return false;
		if (sabeEscribir == null) {
			if (other.sabeEscribir != null)
				return false;
		} else if (!sabeEscribir.equals(other.sabeEscribir))
			return false;
		if (sabeLeer == null) {
			if (other.sabeLeer != null)
				return false;
		} else if (!sabeLeer.equals(other.sabeLeer))
			return false;
		if (segundoApellido == null) {
			if (other.segundoApellido != null)
				return false;
		} else if (!segundoApellido.equals(other.segundoApellido))
			return false;
		if (segundoNombre == null) {
			if (other.segundoNombre != null)
				return false;
		} else if (!segundoNombre.equals(other.segundoNombre))
			return false;
		if (tercerApellido == null) {
			if (other.tercerApellido != null)
				return false;
		} else if (!tercerApellido.equals(other.tercerApellido))
			return false;
		if (tercerNombre == null) {
			if (other.tercerNombre != null)
				return false;
		} else if (!tercerNombre.equals(other.tercerNombre))
			return false;
		if (vecindadDepartamento == null) {
			if (other.vecindadDepartamento != null)
				return false;
		} else if (!vecindadDepartamento.equals(other.vecindadDepartamento))
			return false;
		if (vecindadMunicipio == null) {
			if (other.vecindadMunicipio != null)
				return false;
		} else if (!vecindadMunicipio.equals(other.vecindadMunicipio))
			return false;
		return true;
	}
	
	
}
