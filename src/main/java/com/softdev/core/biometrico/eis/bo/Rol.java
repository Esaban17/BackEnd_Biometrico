package com.softdev.core.biometrico.eis.bo;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
@Table(name="rol",schema = "biometrico")
public class Rol implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "idRol")
	private Long idRol;
	@Column (name = "descripcion",nullable = false)
	private String descripcion;
	
	@OneToMany(mappedBy = "rol",fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<AsignacionPersona> listaAsignacionPersonas;
	
	
	public Rol() {
		
	}


	public Rol(Long idRol, String descripcion, Set<AsignacionPersona> listaAsignacionPersonas) {
		super();
		this.idRol = idRol;
		this.descripcion = descripcion;
		this.listaAsignacionPersonas = listaAsignacionPersonas;
	}


	public Long getIdRol() {
		return idRol;
	}


	public void setIdRol(Long idRol) {
		this.idRol = idRol;
	}


	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public Set<AsignacionPersona> getListaAsignacionPersonas() {
		return listaAsignacionPersonas;
	}


	public void setListaAsignacionPersonas(Set<AsignacionPersona> listaAsignacionPersonas) {
		this.listaAsignacionPersonas = listaAsignacionPersonas;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((idRol == null) ? 0 : idRol.hashCode());
		result = prime * result + ((listaAsignacionPersonas == null) ? 0 : listaAsignacionPersonas.hashCode());
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Rol other = (Rol) obj;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (idRol == null) {
			if (other.idRol != null)
				return false;
		} else if (!idRol.equals(other.idRol))
			return false;
		if (listaAsignacionPersonas == null) {
			if (other.listaAsignacionPersonas != null)
				return false;
		} else if (!listaAsignacionPersonas.equals(other.listaAsignacionPersonas))
			return false;
		return true;
	}	

}
