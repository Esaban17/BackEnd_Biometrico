package com.softdev.core.biometrico.eis.bo;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "asignacionPersona",schema = "biometrico")
public class AsignacionPersona implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idAsignacion")
	private Long idAsignacion;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idPersona")
	//@JsonIgnore
	private Persona persona;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idDepartamento")
	//@JsonIgnore
	private Departamento departamento;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idRol")
	//@JsonIgnore
	private Rol rol;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idNivelAcceso")
	//@JsonIgnore
	private NivelAcceso nivelAcceso;
	
	
	public AsignacionPersona() {
		
	}


	public Long getIdAsignacion() {
		return idAsignacion;
	}


	public void setIdAsignacion(Long idAsignacion) {
		this.idAsignacion = idAsignacion;
	}


	public Persona getPersona() {
		return persona;
	}


	public void setPersona(Persona persona) {
		this.persona = persona;
	}


	public Departamento getDepartamento() {
		return departamento;
	}


	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}


	public Rol getRol() {
		return rol;
	}


	public void setRol(Rol rol) {
		this.rol = rol;
	}


	public NivelAcceso getNivelAcceso() {
		return nivelAcceso;
	}


	public void setNivelAcceso(NivelAcceso nivelAcceso) {
		this.nivelAcceso = nivelAcceso;
	}
	
}
