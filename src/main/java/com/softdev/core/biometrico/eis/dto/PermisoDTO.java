package com.softdev.core.biometrico.eis.dto;
import java.io.Serializable;
import com.softdev.core.biometrico.eis.bo.Permiso;
public class PermisoDTO implements Serializable{
	private Long idPermiso;
	private String descripcion;
	private String codigo;
	
	public PermisoDTO() {
		
	}
	
	public PermisoDTO(Permiso permiso) {
		this.idPermiso = permiso.getIdPermiso();
		this.descripcion = permiso.getDescripcion();
		this.codigo = permiso.getDescripcion();
	}

	public PermisoDTO(Long idPermiso, String descripcion, String codigo) {
		super();
		this.idPermiso = idPermiso;
		this.descripcion = descripcion;
		this.codigo = codigo;
	}

	public Long getIdPermiso() {
		return idPermiso;
	}

	public void setIdPermiso(Long idPermiso) {
		this.idPermiso = idPermiso;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((idPermiso == null) ? 0 : idPermiso.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PermisoDTO other = (PermisoDTO) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (idPermiso == null) {
			if (other.idPermiso != null)
				return false;
		} else if (!idPermiso.equals(other.idPermiso))
			return false;
		return true;
	}
	
	
}
