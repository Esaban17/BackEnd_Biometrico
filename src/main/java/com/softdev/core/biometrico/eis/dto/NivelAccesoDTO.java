package com.softdev.core.biometrico.eis.dto;
import java.io.Serializable;
import com.softdev.core.biometrico.eis.bo.NivelAcceso;
public class NivelAccesoDTO  implements Serializable{
	private Long idNivelAcceso;
	private String descripcion;
	private String codigo;
	
	public NivelAccesoDTO() {
		super();
	}
	
	public NivelAccesoDTO(NivelAcceso nivelAcceso) {
		this.idNivelAcceso = nivelAcceso.getIdNivelAcceso();
		this.descripcion = nivelAcceso.getDescripcion();
		this.codigo = nivelAcceso.getCodigo();
	}

	public NivelAccesoDTO(Long idNivelAcceso, String descripcion, String codigo) {
		super();
		this.idNivelAcceso = idNivelAcceso;
		this.descripcion = descripcion;
		this.codigo = codigo;
	}

	public Long getIdNivelAcceso() {
		return idNivelAcceso;
	}

	public void setIdNivelAcceso(Long idNivelAcceso) {
		this.idNivelAcceso = idNivelAcceso;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((descripcion == null) ? 0 : descripcion.hashCode());
		result = prime * result + ((idNivelAcceso == null) ? 0 : idNivelAcceso.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NivelAccesoDTO other = (NivelAccesoDTO) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (descripcion == null) {
			if (other.descripcion != null)
				return false;
		} else if (!descripcion.equals(other.descripcion))
			return false;
		if (idNivelAcceso == null) {
			if (other.idNivelAcceso != null)
				return false;
		} else if (!idNivelAcceso.equals(other.idNivelAcceso))
			return false;
		return true;
	}
	
	
}
