package com.softdev.core.biometrico.eis.bo;
import java.io.Serializable;
import java.sql.Time;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import com.softdev.core.biometrico.eis.bo.Semana;

@Entity
@Table(name = "horarioPersona",schema = "biometrico")
public class HorarioPersona  implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idHorarioPersona")
	private Long idHorarioPersona;
	
	//@ManyToOne(fetch = FetchType.EAGER)
	@Column(name = "idPersona")
	//@JsonIgnore
	private Long  persona;
	
	@Column(name = "dia")
	private Semana dia;
	@Column(name = "horaEntradaM",nullable = true)
	private Time horaEntradaMatutina;
	@Column(name = "horaSalidaM",nullable = true)
	private Time horaSalidaMatutina;
	@Column(name = "horaEntradaV",nullable = true)
	private Time horaEntradaVespertina;
	@Column(name = "horaSalidaV",nullable = true)
	private Time horaSalidaVespertina;

	public HorarioPersona() {
		
	}

	public Long getIdHorarioPersona() {
		return idHorarioPersona;
	}

	public void setIdHorarioPersona(Long idHorarioPersona) {
		this.idHorarioPersona = idHorarioPersona;
	}

	public Long getPersona() {
		return persona;
	}

	public void setPersona(Long persona) {
		this.persona = persona;
	}

	public Semana getDia() {
		return dia;
	}

	public void setDia(Semana dia) {
		this.dia = dia;
	}

	public Time getHoraEntradaMatutina() {
		return horaEntradaMatutina;
	}

	public void setHoraEntradaMatutina(Time horaEntradaMatutina) {
		this.horaEntradaMatutina = horaEntradaMatutina;
	}

	public Time getHoraSalidaMatutina() {
		return horaSalidaMatutina;
	}

	public void setHoraSalidaMatutina(Time horaSalidaMatutina) {
		this.horaSalidaMatutina = horaSalidaMatutina;
	}

	public Time getHoraEntradaVespertina() {
		return horaEntradaVespertina;
	}

	public void setHoraEntradaVespertina(Time horaEntradaVespertina) {
		this.horaEntradaVespertina = horaEntradaVespertina;
	}

	public Time getHoraSalidaVespertina() {
		return horaSalidaVespertina;
	}

	public void setHoraSalidaVespertina(Time horaSalidaVespertina) {
		this.horaSalidaVespertina = horaSalidaVespertina;
	}
}
