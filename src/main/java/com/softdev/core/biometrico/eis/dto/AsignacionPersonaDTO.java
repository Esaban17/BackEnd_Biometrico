package com.softdev.core.biometrico.eis.dto;
import java.io.Serializable;
import com.softdev.core.biometrico.eis.bo.AsignacionPersona;
import com.softdev.core.biometrico.eis.bo.Departamento;
import com.softdev.core.biometrico.eis.bo.NivelAcceso;
import com.softdev.core.biometrico.eis.bo.Persona;
import com.softdev.core.biometrico.eis.bo.Rol;
public class AsignacionPersonaDTO implements Serializable{
	private Long idAsignacion;
	private Persona persona;
	private Departamento departamento;
	private Rol rol;
	private NivelAcceso nivelAcceso;
	
	public AsignacionPersonaDTO() {
		
	}
	
	public AsignacionPersonaDTO(AsignacionPersona asignacionPersona) {
		this.idAsignacion = asignacionPersona.getIdAsignacion();
		this.persona = asignacionPersona.getPersona();
		this.departamento = asignacionPersona.getDepartamento();
		this.rol = asignacionPersona.getRol();
		this.nivelAcceso = asignacionPersona.getNivelAcceso();
	}

	public AsignacionPersonaDTO(Long idAsignacion, Persona persona, Departamento departamento, Rol rol,
			NivelAcceso nivelAcceso) {
		super();
		this.idAsignacion = idAsignacion;
		this.persona = persona;
		this.departamento = departamento;
		this.rol = rol;
		this.nivelAcceso = nivelAcceso;
	}

	public Long getIdAsignacion() {
		return idAsignacion;
	}

	public void setIdAsignacion(Long idAsignacion) {
		this.idAsignacion = idAsignacion;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Departamento getDepartamento() {
		return departamento;
	}

	public void setDepartamento(Departamento departamento) {
		this.departamento = departamento;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public NivelAcceso getNivelAcceso() {
		return nivelAcceso;
	}

	public void setNivelAcceso(NivelAcceso nivelAcceso) {
		this.nivelAcceso = nivelAcceso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((departamento == null) ? 0 : departamento.hashCode());
		result = prime * result + ((idAsignacion == null) ? 0 : idAsignacion.hashCode());
		result = prime * result + ((nivelAcceso == null) ? 0 : nivelAcceso.hashCode());
		result = prime * result + ((persona == null) ? 0 : persona.hashCode());
		result = prime * result + ((rol == null) ? 0 : rol.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AsignacionPersonaDTO other = (AsignacionPersonaDTO) obj;
		if (departamento == null) {
			if (other.departamento != null)
				return false;
		} else if (!departamento.equals(other.departamento))
			return false;
		if (idAsignacion == null) {
			if (other.idAsignacion != null)
				return false;
		} else if (!idAsignacion.equals(other.idAsignacion))
			return false;
		if (nivelAcceso == null) {
			if (other.nivelAcceso != null)
				return false;
		} else if (!nivelAcceso.equals(other.nivelAcceso))
			return false;
		if (persona == null) {
			if (other.persona != null)
				return false;
		} else if (!persona.equals(other.persona))
			return false;
		if (rol == null) {
			if (other.rol != null)
				return false;
		} else if (!rol.equals(other.rol))
			return false;
		return true;
	}
}
