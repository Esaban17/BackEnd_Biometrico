package com.softdev.core.biometrico.eis.bo;
import java.io.Serializable;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
@Table(name="departamento",schema = "biometrico")
public class Departamento implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "idDepartamento")
	private Long idDepartamento;
	@Column (name = "nombreDepartamento",nullable = false)
	private String nombreDepartamento;
	
	public Departamento() {
		
	}
	
	@OneToMany(mappedBy = "departamento",fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<AsignacionPersona> listaAsignacionPersonas;

	
	public Long getIdDepartamento() {
		return idDepartamento;
	}

	public void setIdDepartamento(Long idDepartamento) {
		this.idDepartamento = idDepartamento;
	}

	public String getNombreDepartamento() {
		return nombreDepartamento;
	}

	public void setNombreDepartamento(String nombreDepartamento) {
		this.nombreDepartamento = nombreDepartamento;
	}

	public Set<AsignacionPersona> getListaAsignacionPersonas() {
		return listaAsignacionPersonas;
	}

	public void setListaAsignacionPersonas(Set<AsignacionPersona> listaAsignacionPersonas) {
		this.listaAsignacionPersonas = listaAsignacionPersonas;
	}
}
