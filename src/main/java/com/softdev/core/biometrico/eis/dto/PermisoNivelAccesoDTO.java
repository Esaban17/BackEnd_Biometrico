package com.softdev.core.biometrico.eis.dto;
import java.io.Serializable;
import com.softdev.core.biometrico.eis.bo.NivelAcceso;
import com.softdev.core.biometrico.eis.bo.Permiso;
import com.softdev.core.biometrico.eis.bo.PermisoNivelAcceso;
public class PermisoNivelAccesoDTO implements Serializable {
	private Long idPermisoNivelAcceso;
	private NivelAcceso nivelAcceso;
	private Permiso permiso;
	
	public PermisoNivelAccesoDTO() {
		
	}
	
	public PermisoNivelAccesoDTO(PermisoNivelAcceso permisoNivelAcceso) {
		this.idPermisoNivelAcceso = permisoNivelAcceso.getIdPermisoNivelAcceso();
		this.nivelAcceso = permisoNivelAcceso.getNivelAcceso();
		this.permiso = permisoNivelAcceso.getPermiso();
	}

	public PermisoNivelAccesoDTO(Long idPermisoNivelAcceso, NivelAcceso nivelAcceso, Permiso permiso) {
		super();
		this.idPermisoNivelAcceso = idPermisoNivelAcceso;
		this.nivelAcceso = nivelAcceso;
		this.permiso = permiso;
	}

	public Long getIdPermisoNivelAcceso() {
		return idPermisoNivelAcceso;
	}

	public void setIdPermisoNivelAcceso(Long idPermisoNivelAcceso) {
		this.idPermisoNivelAcceso = idPermisoNivelAcceso;
	}

	public NivelAcceso getNivelAcceso() {
		return nivelAcceso;
	}

	public void setNivelAcceso(NivelAcceso nivelAcceso) {
		this.nivelAcceso = nivelAcceso;
	}

	public Permiso getPermiso() {
		return permiso;
	}

	public void setPermiso(Permiso permiso) {
		this.permiso = permiso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPermisoNivelAcceso == null) ? 0 : idPermisoNivelAcceso.hashCode());
		result = prime * result + ((nivelAcceso == null) ? 0 : nivelAcceso.hashCode());
		result = prime * result + ((permiso == null) ? 0 : permiso.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PermisoNivelAccesoDTO other = (PermisoNivelAccesoDTO) obj;
		if (idPermisoNivelAcceso == null) {
			if (other.idPermisoNivelAcceso != null)
				return false;
		} else if (!idPermisoNivelAcceso.equals(other.idPermisoNivelAcceso))
			return false;
		if (nivelAcceso == null) {
			if (other.nivelAcceso != null)
				return false;
		} else if (!nivelAcceso.equals(other.nivelAcceso))
			return false;
		if (permiso == null) {
			if (other.permiso != null)
				return false;
		} else if (!permiso.equals(other.permiso))
			return false;
		return true;
	}
	
}
