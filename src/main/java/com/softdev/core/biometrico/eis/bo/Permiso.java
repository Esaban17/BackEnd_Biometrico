package com.softdev.core.biometrico.eis.bo;
import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
@Table(name="permiso", schema="biometrico")
public class Permiso implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column (name = "idPermiso")
	private Long idPermiso;
	@Column (name = "descripcion",nullable = false)
	private String descripcion;
	@Column (name = "codigo",nullable = false)
	private String codigo;
	
	@OneToMany(mappedBy = "permiso",fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<PermisoNivelAcceso> listaPermisoNivelAccesos;
	
	public Permiso () {
		
	}

	public Long getIdPermiso() {
		return idPermiso;
	}

	public void setIdPermiso(Long idPermiso) {
		this.idPermiso = idPermiso;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Set<PermisoNivelAcceso> getListaPermisoNivelAccesos() {
		return listaPermisoNivelAccesos;
	}

	public void setListaPermisoNivelAccesos(Set<PermisoNivelAcceso> listaPermisoNivelAccesos) {
		this.listaPermisoNivelAccesos = listaPermisoNivelAccesos;
	}
	
}
