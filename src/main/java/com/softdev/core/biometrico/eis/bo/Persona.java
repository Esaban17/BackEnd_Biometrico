package com.softdev.core.biometrico.eis.bo;
import java.io.Serializable;
import java.sql.Date;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
@Table(name = "persona",schema = "biometrico")
public class Persona implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idPersona")
	private Long idPersona;
	@Column(name = "activo",nullable = false)
	private int activo;
	@Column(name = "cui",nullable = false)
	private Long cui;
	@Column(name = "primerNombre",nullable = false)
	private String primerNombre;
	@Column(name = "segundoNombre")
	private String segundoNombre;
	@Column(name = "tercerNombre")
	private String tercerNombre;
	@Column(name = "primerApellido",nullable = false)
	private String primerApellido;
	@Column(name = "segundoApellido")
	private String segundoApellido;
	@Column(name = "tercerApellido")
	private String tercerApellido;
	@Column(name = "genero",nullable = false)
	private String genero;
	@Column(name = "nacionalidadMunicipio")
	private String nacionalidadMunicipio;
	@Column(name = "nacionalidadDepartamento")
	private String nacionalidadDepartamento;
	@Column(name = "nacionalidadPais")
	private String nacionalidadPais;
	@Column(name = "fechaNacimiento")
	private Date fechaNacimiento;
	@Column(name = "fechaEmisionDocumento")
	private Date fechaEmisionDocumento;
	@Column(name = "fechaVencimientoDocumento")
	private Date fechaVencimientoDocumento;
	@Column(name = "estadoCivil",nullable = false)
	private String estadoCivil;
	@Column(name = "vecindadMunicipio")
	private String vecindadMunicipio;
	@Column(name = "vecindadDepartamento")
	private String vecindadDepartamento;
	@Column(name = "nacionalidad")
	private String nacionalidad;
	@Column(name = "sabeLeer",nullable = false)
	private String sabeLeer;
	@Column(name = "sabeEscribir",nullable = false)
	private String sabeEscribir;
	@Column(name = "limitacionesFisicas")
	private String limitacionesFisicas;
	@Column(name = "libro")
	private String libro;
	@Column(name = "folio")
	private String folio;
	@Column(name = "partida")
	private String partida;
	@Column(name = "residenciaDireccion")
	private String residenciaDireccion;
	@Column(name = "residenciaMunicipio")
	private String residenciaMunicipio;
	@Column(name = "residenciaDepartamento")
	private String residenciaDepartamento;
	@Column(name = "codigoPostal")
	private String codigoPostal;
	@Column(name = "residenciaPais")
	private String residenciaPais;
	@Column(name = "profesion",nullable = false)
	private String profesion;
	@Column(name = "cedulaNumero")
	private String cedulaNumero;
	@Column(name = "cedulaMunicipio")
	private String cedulaMunicipio;
	@Column(name = "cedulaDepartamento")
	private String cedulaDepartamento;
	@Column(name = "oficialActivo")
	private String oficialActivo;
	@Column(name = "fotografia",nullable = true)
	private byte fotografia;
	@Column(name = "huellaDactilar",nullable = true)
	private byte huellaDactilar;
	@Column(name = "MRZ")
	private String MRZ;
	@Column(name = "numeroSerieDocumento")
	private String numeroSerieDocumento;
	@Column(name = "correoPersonal")
	private String correoPersonal;
	@Column(name = "correoInstitucional")
	private String correoInstitucional;
	@Column(name = "numeroTelefonico")
	private String numeroTelefonico;
	@Column(name = "numeroTelefonicoMovil")
	private String numeroTelefonicoMovil;
	
	@OneToMany(mappedBy = "persona",fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<Asistencia> listaAsistencias;
	
	@OneToMany(mappedBy = "persona",fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<Usuario> listaUsuarios;
	
	@OneToMany(mappedBy = "persona",fetch = FetchType.EAGER)
	@JsonIgnore
	private Set<AsignacionPersona> listaAsignacionPersonas;
	
	public Persona() {
		
	}

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public int getActivo() {
		return activo;
	}

	public void setActivo(int activo) {
		this.activo = activo;
	}

	public Long getCui() {
		return cui;
	}

	public void setCui(Long cui) {
		this.cui = cui;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getSegundoNombre() {
		return segundoNombre;
	}

	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}

	public String getTercerNombre() {
		return tercerNombre;
	}

	public void setTercerNombre(String tercerNombre) {
		this.tercerNombre = tercerNombre;
	}

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getTercerApellido() {
		return tercerApellido;
	}

	public void setTercerApellido(String tercerApellido) {
		this.tercerApellido = tercerApellido;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getNacionalidadMunicipio() {
		return nacionalidadMunicipio;
	}

	public void setNacionalidadMunicipio(String nacionalidadMunicipio) {
		this.nacionalidadMunicipio = nacionalidadMunicipio;
	}

	public String getNacionalidadDepartamento() {
		return nacionalidadDepartamento;
	}

	public void setNacionalidadDepartamento(String nacionalidadDepartamento) {
		this.nacionalidadDepartamento = nacionalidadDepartamento;
	}

	public String getNacionalidadPais() {
		return nacionalidadPais;
	}

	public void setNacionalidadPais(String nacionalidadPais) {
		this.nacionalidadPais = nacionalidadPais;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public Date getFechaEmisionDocumento() {
		return fechaEmisionDocumento;
	}

	public void setFechaEmisionDocumento(Date fechaEmisionDocumento) {
		this.fechaEmisionDocumento = fechaEmisionDocumento;
	}

	public Date getFechaVencimientoDocumento() {
		return fechaVencimientoDocumento;
	}

	public void setFechaVencimientoDocumento(Date fechaVencimientoDocumento) {
		this.fechaVencimientoDocumento = fechaVencimientoDocumento;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public String getVecindadMunicipio() {
		return vecindadMunicipio;
	}

	public void setVecindadMunicipio(String vecindadMunicipio) {
		this.vecindadMunicipio = vecindadMunicipio;
	}

	public String getVecindadDepartamento() {
		return vecindadDepartamento;
	}

	public void setVecindadDepartamento(String vecindadDepartamento) {
		this.vecindadDepartamento = vecindadDepartamento;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getSabeLeer() {
		return sabeLeer;
	}

	public void setSabeLeer(String sabeLeer) {
		this.sabeLeer = sabeLeer;
	}

	public String getSabeEscribir() {
		return sabeEscribir;
	}

	public void setSabeEscribir(String sabeEscribir) {
		this.sabeEscribir = sabeEscribir;
	}

	public String getLimitacionesFisicas() {
		return limitacionesFisicas;
	}

	public void setLimitacionesFisicas(String limitacionesFisicas) {
		this.limitacionesFisicas = limitacionesFisicas;
	}

	public String getLibro() {
		return libro;
	}

	public void setLibro(String libro) {
		this.libro = libro;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getPartida() {
		return partida;
	}

	public void setPartida(String partida) {
		this.partida = partida;
	}

	public String getResidenciaDireccion() {
		return residenciaDireccion;
	}

	public void setResidenciaDireccion(String residenciaDireccion) {
		this.residenciaDireccion = residenciaDireccion;
	}

	public String getResidenciaMunicipio() {
		return residenciaMunicipio;
	}

	public void setResidenciaMunicipio(String residenciaMunicipio) {
		this.residenciaMunicipio = residenciaMunicipio;
	}

	public String getResidenciaDepartamento() {
		return residenciaDepartamento;
	}

	public void setResidenciaDepartamento(String residenciaDepartamento) {
		this.residenciaDepartamento = residenciaDepartamento;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public String getResidenciaPais() {
		return residenciaPais;
	}

	public void setResidenciaPais(String residenciaPais) {
		this.residenciaPais = residenciaPais;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getCedulaNumero() {
		return cedulaNumero;
	}

	public void setCedulaNumero(String cedulaNumero) {
		this.cedulaNumero = cedulaNumero;
	}

	public String getCedulaMunicipio() {
		return cedulaMunicipio;
	}

	public void setCedulaMunicipio(String cedulaMunicipio) {
		this.cedulaMunicipio = cedulaMunicipio;
	}

	public String getCedulaDepartamento() {
		return cedulaDepartamento;
	}

	public void setCedulaDepartamento(String cedulaDepartamento) {
		this.cedulaDepartamento = cedulaDepartamento;
	}

	public String getOficialActivo() {
		return oficialActivo;
	}

	public void setOficialActivo(String oficialActivo) {
		this.oficialActivo = oficialActivo;
	}

	public byte getFotografia() {
		return fotografia;
	}

	public void setFotografia(byte fotografia) {
		this.fotografia = fotografia;
	}

	public byte getHuellaDactilar() {
		return huellaDactilar;
	}

	public void setHuellaDactilar(byte huellaDactilar) {
		this.huellaDactilar = huellaDactilar;
	}

	public String getMRZ() {
		return MRZ;
	}

	public void setMRZ(String mRZ) {
		MRZ = mRZ;
	}

	public String getNumeroSerieDocumento() {
		return numeroSerieDocumento;
	}

	public void setNumeroSerieDocumento(String numeroSerieDocumento) {
		this.numeroSerieDocumento = numeroSerieDocumento;
	}

	public String getCorreoPersonal() {
		return correoPersonal;
	}

	public void setCorreoPersonal(String correoPersonal) {
		this.correoPersonal = correoPersonal;
	}

	public String getCorreoInstitucional() {
		return correoInstitucional;
	}

	public void setCorreoInstitucional(String correoInstitucional) {
		this.correoInstitucional = correoInstitucional;
	}

	public String getNumeroTelefonico() {
		return numeroTelefonico;
	}

	public void setNumeroTelefonico(String numeroTelefonico) {
		this.numeroTelefonico = numeroTelefonico;
	}

	public String getNumeroTelefonicoMovil() {
		return numeroTelefonicoMovil;
	}

	public void setNumeroTelefonicoMovil(String numeroTelefonicoMovil) {
		this.numeroTelefonicoMovil = numeroTelefonicoMovil;
	}

	public Set<Asistencia> getListaAsistencias() {
		return listaAsistencias;
	}

	public void setListaAsistencias(Set<Asistencia> listaAsistencias) {
		this.listaAsistencias = listaAsistencias;
	}

	public Set<Usuario> getListaUsuarios() {
		return listaUsuarios;
	}

	public void setListaUsuarios(Set<Usuario> listaUsuarios) {
		this.listaUsuarios = listaUsuarios;
	}

	public Set<AsignacionPersona> getListaAsignacionPersonas() {
		return listaAsignacionPersonas;
	}

	public void setListaAsignacionPersonas(Set<AsignacionPersona> listaAsignacionPersonas) {
		this.listaAsignacionPersonas = listaAsignacionPersonas;
	}
}
