package com.softdev.core.biometrico.eis.dto;
import java.io.Serializable;
import com.softdev.core.biometrico.eis.bo.Departamento;
public class DepartamentoDTO implements Serializable {
	private Long idDepartamento;
	private String nombreDepartamento;
	
	public DepartamentoDTO() {
		super();
	}
	public DepartamentoDTO(Departamento departamento) {
		this.idDepartamento=departamento.getIdDepartamento();
		this.nombreDepartamento=departamento.getNombreDepartamento();
	}
	
	public DepartamentoDTO(Long idDepartamento, String nombreDepartamento) {
		super();
		this.idDepartamento = idDepartamento;
		this.nombreDepartamento = nombreDepartamento;
	}
	
	public Long getIdDepartamento() {
		return idDepartamento;
	}
	
	public void setIdDepartamento(Long idDepartamento) {
		this.idDepartamento = idDepartamento;
	}
	
	public String getNombreDepartamento() {
		return nombreDepartamento;
	}
	
	public void setNombreDepartamento(String nombreDepartamento) {
		this.nombreDepartamento = nombreDepartamento;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idDepartamento == null) ? 0 : idDepartamento.hashCode());
		result = prime * result + ((nombreDepartamento == null) ? 0 : nombreDepartamento.hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DepartamentoDTO other = (DepartamentoDTO) obj;
		if (idDepartamento == null) {
			if (other.idDepartamento != null)
				return false;
		} else if (!idDepartamento.equals(other.idDepartamento))
			return false;
		if (nombreDepartamento == null) {
			if (other.nombreDepartamento != null)
				return false;
		} else if (!nombreDepartamento.equals(other.nombreDepartamento))
			return false;
		return true;
	}
	
	
	

}
