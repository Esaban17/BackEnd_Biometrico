package com.softdev.core.biometrico.eis.bo;
import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GenerationType;
import javax.persistence.GeneratedValue;
import javax.persistence.Table;
import com.fasterxml.jackson.annotation.JsonIgnore;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name = "permisoNivelAcceso", schema = "biometrioco") 
public class PermisoNivelAcceso {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idPermisoNivelAcceso")
	private Long idPermisoNivelAcceso;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idNivelAcceso")
	//@JsonIgnore
	private NivelAcceso nivelAcceso;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "idPermiso")
	//@JsonIgnore
	private Permiso permiso;
	
	public PermisoNivelAcceso() {
		
	}
	
	public PermisoNivelAcceso(Long idPermisoNivelAcceso, NivelAcceso nivelAcceso, Permiso permiso) {
		super();
		this.idPermisoNivelAcceso = idPermisoNivelAcceso;
		this.nivelAcceso = nivelAcceso;
		this.permiso = permiso;
	}

	public Long getIdPermisoNivelAcceso() {
		return idPermisoNivelAcceso;
	}

	public void setIdPermisoNivelAcceso(Long idPermisoNivelAcceso) {
		this.idPermisoNivelAcceso = idPermisoNivelAcceso;
	}

	public NivelAcceso getNivelAcceso() {
		return nivelAcceso;
	}

	public void setNivelAcceso(NivelAcceso nivelAcceso) {
		this.nivelAcceso = nivelAcceso;
	}

	public Permiso getPermiso() {
		return permiso;
	}

	public void setPermiso(Permiso permiso) {
		this.permiso = permiso;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idPermisoNivelAcceso == null) ? 0 : idPermisoNivelAcceso.hashCode());
		result = prime * result + ((nivelAcceso == null) ? 0 : nivelAcceso.hashCode());
		result = prime * result + ((permiso == null) ? 0 : permiso.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PermisoNivelAcceso other = (PermisoNivelAcceso) obj;
		if (idPermisoNivelAcceso == null) {
			if (other.idPermisoNivelAcceso != null)
				return false;
		} else if (!idPermisoNivelAcceso.equals(other.idPermisoNivelAcceso))
			return false;
		if (nivelAcceso == null) {
			if (other.nivelAcceso != null)
				return false;
		} else if (!nivelAcceso.equals(other.nivelAcceso))
			return false;
		if (permiso == null) {
			if (other.permiso != null)
				return false;
		} else if (!permiso.equals(other.permiso))
			return false;
		return true;
	}
	
	
}
