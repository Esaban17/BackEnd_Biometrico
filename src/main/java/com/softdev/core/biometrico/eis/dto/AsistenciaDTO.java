package com.softdev.core.biometrico.eis.dto;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import com.softdev.core.biometrico.eis.bo.Asistencia;
import com.softdev.core.biometrico.eis.bo.Persona;
public class AsistenciaDTO implements Serializable{
	private Long idAsistencia;
	private Persona persona;
	private Date fecha;
	private Time horaEntrada;
	private Time horaSalida;
	
	public AsistenciaDTO() {
		
	}
	
	public AsistenciaDTO(Asistencia asistencia) {
		this.idAsistencia = asistencia.getIdAsistencia();
		this.persona = asistencia.getPersona();
		this.fecha = asistencia.getFecha();
		this.horaEntrada = asistencia.getHoraEntrada();
		this.horaSalida = asistencia.getHoraSalida();
	}

	public AsistenciaDTO(Long idAsistencia, Persona persona, Date fecha, Time horaEntrada, Time horaSalida) {
		super();
		this.idAsistencia = idAsistencia;
		this.persona = persona;
		this.fecha = fecha;
		this.horaEntrada = horaEntrada;
		this.horaSalida = horaSalida;
	}

	public Long getIdAsistencia() {
		return idAsistencia;
	}

	public void setIdAsistencia(Long idAsistencia) {
		this.idAsistencia = idAsistencia;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Time getHoraEntrada() {
		return horaEntrada;
	}

	public void setHoraEntrada(Time horaEntrada) {
		this.horaEntrada = horaEntrada;
	}

	public Time getHoraSalida() {
		return horaSalida;
	}

	public void setHoraSalida(Time horaSalida) {
		this.horaSalida = horaSalida;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fecha == null) ? 0 : fecha.hashCode());
		result = prime * result + ((horaEntrada == null) ? 0 : horaEntrada.hashCode());
		result = prime * result + ((horaSalida == null) ? 0 : horaSalida.hashCode());
		result = prime * result + ((idAsistencia == null) ? 0 : idAsistencia.hashCode());
		result = prime * result + ((persona == null) ? 0 : persona.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		AsistenciaDTO other = (AsistenciaDTO) obj;
		if (fecha == null) {
			if (other.fecha != null)
				return false;
		} else if (!fecha.equals(other.fecha))
			return false;
		if (horaEntrada == null) {
			if (other.horaEntrada != null)
				return false;
		} else if (!horaEntrada.equals(other.horaEntrada))
			return false;
		if (horaSalida == null) {
			if (other.horaSalida != null)
				return false;
		} else if (!horaSalida.equals(other.horaSalida))
			return false;
		if (idAsistencia == null) {
			if (other.idAsistencia != null)
				return false;
		} else if (!idAsistencia.equals(other.idAsistencia))
			return false;
		if (persona == null) {
			if (other.persona != null)
				return false;
		} else if (!persona.equals(other.persona))
			return false;
		return true;
	}
}
