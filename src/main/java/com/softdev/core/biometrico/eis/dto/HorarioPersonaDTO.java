package com.softdev.core.biometrico.eis.dto;
import java.io.Serializable;
import java.sql.Time;
import com.softdev.core.biometrico.eis.bo.HorarioPersona;
import com.softdev.core.biometrico.eis.bo.Persona;
import com.softdev.core.biometrico.eis.bo.Semana;
public class HorarioPersonaDTO implements Serializable{
	private Long idHorarioPersona;
	private Persona persona;
	private Semana dia;
	private Time horaEntradaMatutina;
	private Time horaSalidaMatutina;
	private Time horaEntradaVespertina;
	private Time horaSalidaVespertina;
	
	public HorarioPersonaDTO() {
		
	}

	public HorarioPersonaDTO(Long idHorarioPersona, Persona persona, String dia, Time horaEntradaMatutina,
			Time horaSalidaMatutina, Time horaEntradaVespertina, Time horaSalidaVespertina) {
		super();
		this.idHorarioPersona = idHorarioPersona;
		this.persona = persona;
		this.dia = this.dia.valueOf(dia);
		this.horaEntradaMatutina = horaEntradaMatutina;
		this.horaSalidaMatutina = horaSalidaMatutina;
		this.horaEntradaVespertina = horaEntradaVespertina;
		this.horaSalidaVespertina = horaSalidaVespertina;
	}

	public Long getIdHorarioPersona() {
		return idHorarioPersona;
	}

	public void setIdHorarioPersona(Long idHorarioPersona) {
		this.idHorarioPersona = idHorarioPersona;
	}

	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Semana getDia() {
		return dia;
	}

	public void setDia(Semana dia) {
		this.dia = dia;
	}

	public Time getHoraEntradaMatutina() {
		return horaEntradaMatutina;
	}

	public void setHoraEntradaMatutina(Time horaEntradaMatutina) {
		this.horaEntradaMatutina = horaEntradaMatutina;
	}

	public Time getHoraSalidaMatutina() {
		return horaSalidaMatutina;
	}

	public void setHoraSalidaMatutina(Time horaSalidaMatutina) {
		this.horaSalidaMatutina = horaSalidaMatutina;
	}

	public Time getHoraEntradaVespertina() {
		return horaEntradaVespertina;
	}

	public void setHoraEntradaVespertina(Time horaEntradaVespertina) {
		this.horaEntradaVespertina = horaEntradaVespertina;
	}

	public Time getHoraSalidaVespertina() {
		return horaSalidaVespertina;
	}

	public void setHoraSalidaVespertina(Time horaSalidaVespertina) {
		this.horaSalidaVespertina = horaSalidaVespertina;
	}
}
