package com.softdev.core.biometrico.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.ArrayList;
import java.lang.Long;
import com.softdev.core.biometrico.service.NivelAccesoService;
import com.softdev.core.biometrico.eis.bo.NivelAcceso;

@Controller
@RequestMapping("/api/v1")
public class NivelAccesoController {
	@Autowired
	private NivelAccesoService nivelAccesoService;

	// GET
	@RequestMapping(value = "/nivelesAcceso", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<NivelAcceso>> getNivelesAcceso(
			@RequestParam(value = "codigo", required = false) String codigo) {
		List<NivelAcceso> listaNivelesAcceso = new ArrayList<NivelAcceso>();
		if (codigo == null) {
			listaNivelesAcceso = nivelAccesoService.findAllNivelesAcceso();
			if (listaNivelesAcceso.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<List<NivelAcceso>>(listaNivelesAcceso, HttpStatus.OK);
		} else {
			NivelAcceso nivelAcceso = nivelAccesoService.findByName(codigo);
			if (nivelAcceso == null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			listaNivelesAcceso.add(nivelAcceso);
		}
		return new ResponseEntity<List<NivelAcceso>>(listaNivelesAcceso, HttpStatus.OK);
	}

	// GET
	@RequestMapping(value = "/nivelAcceso/{idNivelAcceso}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<NivelAcceso> getNivelAccesoById(@PathVariable("idNivelAcceso") Long idNivelAcceso) {
		NivelAcceso nivelAcceso = nivelAccesoService.findById(idNivelAcceso);
		if (nivelAcceso == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<NivelAcceso>(nivelAcceso, HttpStatus.OK);
	}

	// DELETE
	@RequestMapping(value = "/nivelAcceso/{idNivelAcceso}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public ResponseEntity<?> deleteNivelAccesoById(@PathVariable("idNivelAcceso") Long idNivelAcceso) {
		NivelAcceso nivelAcceso = nivelAccesoService.findById(idNivelAcceso);
		if (nivelAcceso == null) {
			return new ResponseEntity("Error, No existe el Nivel de Acceso con el id " + idNivelAcceso,
					HttpStatus.NOT_FOUND);
		}
		nivelAccesoService.deleteNivelAccesoById(nivelAcceso.getIdNivelAcceso());
		return new ResponseEntity<NivelAcceso>(HttpStatus.NO_CONTENT);
	}

	// CREATE
	@RequestMapping(value = "/nivelAcceso", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> createNivelAcceso(@RequestBody NivelAcceso nivelAcceso,
			UriComponentsBuilder uriComponentsBuilder) {
		if (nivelAccesoService.findByName(nivelAcceso.getDescripcion()) != null) {
			return new ResponseEntity("Error al Crear el registro, ya existe un Nivel de Acceso con esa Descripcion "
					+ nivelAcceso.getDescripcion(), HttpStatus.CONFLICT);
		}
		nivelAccesoService.saveNivelAcceso(nivelAcceso);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/api/v1/nivelAcceso/{idNivelAcceso}")
				.buildAndExpand(nivelAcceso.getIdNivelAcceso()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// UPDATE
	@RequestMapping(value = "/nivelAcceso", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseEntity<NivelAcceso> updateNivelAcceso(@RequestBody NivelAcceso nivelAcceso,
			UriComponentsBuilder uriComponentsBuilder) {
		if (nivelAccesoService.findByName(nivelAcceso.getDescripcion()) != null) {
			return new ResponseEntity("Error al actualizar el registro, un Nivel de Acceso existe con esa Descripcion "
					+ nivelAcceso.getDescripcion(), HttpStatus.CONFLICT);
		}
		nivelAccesoService.updateNivelAcceso(nivelAcceso);
		return new ResponseEntity<NivelAcceso>(nivelAcceso, HttpStatus.OK);
	}

}