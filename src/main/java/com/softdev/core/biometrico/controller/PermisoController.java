package com.softdev.core.biometrico.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.List;
import java.util.ArrayList;
import java.lang.Long;
import com.softdev.core.biometrico.service.PermisoService;
import com.softdev.core.biometrico.eis.bo.Permiso;

@Controller
@RequestMapping("/api/v1")
public class PermisoController {
	@Autowired
	private PermisoService permisoService;

	// GET
	@RequestMapping(value = "/permisos", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Permiso>> getPermisos(@RequestParam(value = "codigo", required = false) String codigo) {
		List<Permiso> listaPermisos = new ArrayList<Permiso>();
		if (codigo == null) {
			listaPermisos = permisoService.findAllPermisos();
			if (listaPermisos.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<List<Permiso>>(listaPermisos, HttpStatus.OK);
		} else {
			Permiso permiso = permisoService.findByName(codigo);
			if (permiso == null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			listaPermisos.add(permiso);
		}
		return new ResponseEntity<List<Permiso>>(listaPermisos, HttpStatus.OK);
	}

	// GET
	@RequestMapping(value = "/permiso/{idPermiso}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<Permiso> getPermisoById(@PathVariable("idPermiso") Long idPermiso) {
		Permiso permiso = permisoService.findById(idPermiso);
		if (permiso == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Permiso>(permiso, HttpStatus.OK);
	}

	// DELETE
	@RequestMapping(value = "/permiso/{idPermiso}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public ResponseEntity<?> deletePermisoById(@PathVariable("idPermiso") Long idPermiso) {
		Permiso permiso = permisoService.findById(idPermiso);
		if (permiso == null) {
			return new ResponseEntity("Error, No Existe el Permiso, con el id " + idPermiso, HttpStatus.NOT_FOUND);
		}
		permisoService.deletePermisoById(permiso.getIdPermiso());
		return new ResponseEntity<Permiso>(HttpStatus.NO_CONTENT);
	}

	// CREATE
	@RequestMapping(value = "/permiso", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> createPermiso(@RequestBody Permiso permiso, UriComponentsBuilder uriComponentsBuilder) {
		if (permisoService.findByName(permiso.getDescripcion()) != null) {
			return new ResponseEntity(
					"Error al Crear el registro, ya existe un Permiso con esa descripcion" + permiso.getDescripcion(),
					HttpStatus.CONFLICT);
		}
		permisoService.savePermiso(permiso);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/api/v1/permiso/{idPermiso}")
				.buildAndExpand(permiso.getIdPermiso()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// UPDATE
	@RequestMapping(value = "/permiso", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseEntity<Permiso> updatePermiso(@RequestBody Permiso permiso,
			UriComponentsBuilder uriComponentsBuilder) {
		if (permisoService.findByName(permiso.getDescripcion()) != null) {
			return new ResponseEntity("Error al actualizar el registro, un Permiso existe con esa descripcion "
					+ permiso.getDescripcion(), HttpStatus.CONFLICT);
		}
		permisoService.updatePermiso(permiso);
		return new ResponseEntity<Permiso>(permiso, HttpStatus.OK);
	}
}
