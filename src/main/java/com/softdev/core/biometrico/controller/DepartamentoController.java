package com.softdev.core.biometrico.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.List;
import java.util.ArrayList;
import com.softdev.core.biometrico.service.DepartamentoService;
import com.softdev.core.biometrico.eis.bo.Departamento;

@Controller
@RequestMapping("/api/v1")
public class DepartamentoController {
	@Autowired
	private DepartamentoService departamentoService;

	// GET
	@RequestMapping(value = "/departamentos", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Departamento>> getDepartamentos(
			@RequestParam(value = "nombreDepartamento", required = false) String nombreDepartamento) {
		List<Departamento> listaDepartamentos = new ArrayList<Departamento>();
		if (nombreDepartamento == null) {
			listaDepartamentos = departamentoService.findAllDepartamentos();
			if (listaDepartamentos.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<List<Departamento>>(listaDepartamentos, HttpStatus.OK);
		} else {
			Departamento departamento = departamentoService.findByName(nombreDepartamento);
			if (departamento == null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			listaDepartamentos.add(departamento);
		}
		return new ResponseEntity<List<Departamento>>(listaDepartamentos, HttpStatus.OK);
	}

	// GET
	@RequestMapping(value = "/departamento/{idDepartamento}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<Departamento> getDepartamentoById(@PathVariable("idDepartamento") Long idDepartamento) {
		Departamento departamento = departamentoService.findById(idDepartamento);
		if (departamento == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Departamento>(departamento, HttpStatus.OK);
	}

	// DELETE
	@RequestMapping(value = "/departamento/{idDepartamento}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public ResponseEntity<?> deleteDepartamento(@PathVariable("idDepartamento") Long idDepartamento) {
		Departamento departamento = departamentoService.findById(idDepartamento);
		if (departamento == null) {
			return new ResponseEntity("Error, No existe el departamento con el id " + idDepartamento,
					HttpStatus.NOT_FOUND);
		}
		departamentoService.deleteDepartamentoById(departamento.getIdDepartamento());
		return new ResponseEntity<Departamento>(HttpStatus.NO_CONTENT);
	}

	// CREATE
	@RequestMapping(value = "/departamento", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> createDepartamento(@RequestBody Departamento departamento,
			UriComponentsBuilder uriComponentsBuilder) {
		if (departamentoService.findByName(departamento.getNombreDepartamento()) != null) {
			return new ResponseEntity("Error al Crear el registro, ya existe un departamento con el nombre "
					+ departamento.getNombreDepartamento(), HttpStatus.CONFLICT);
		}
		departamentoService.saveDepartamento(departamento);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/api/v1/departamento/{idDepartamento}")
				.buildAndExpand(departamento.getIdDepartamento()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// UPDATE
	@RequestMapping(value = "/departamento", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseEntity<Departamento> updateDepartamento(@RequestBody Departamento departamento,
			UriComponentsBuilder uriComponentsBuilder) {
		if (departamentoService.findByName(departamento.getNombreDepartamento()) != null) {
			return new ResponseEntity("Error al actualizar el registro, un departamento existe con ese nombre"
					+ departamento.getNombreDepartamento(), HttpStatus.CONFLICT);
		}
		departamentoService.updateDepartamento(departamento);
		return new ResponseEntity<Departamento>(departamento, HttpStatus.OK);
	}

}
