package com.softdev.core.biometrico.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.List;
import java.util.ArrayList;
import java.lang.Long;
import com.softdev.core.biometrico.service.UsuarioService;
import com.softdev.core.biometrico.eis.bo.Asistencia;
import com.softdev.core.biometrico.eis.bo.Usuario;

@Controller
@RequestMapping("/api/v1")
public class UsuarioController {
	@Autowired
	private UsuarioService usuarioService;

	// GET
	@RequestMapping(value = "/usuarios", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Usuario>> getUsuarios(
			@RequestParam(value = "usuario", required = false) String usuario) {
		List<Usuario> listaUsuarios = new ArrayList<Usuario>();
		if (usuario == null) {
			listaUsuarios = usuarioService.findAllUsuarios();
			if (listaUsuarios.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<List<Usuario>>(listaUsuarios, HttpStatus.OK);
		} else {
			Usuario user = usuarioService.findByUser(usuario);
			if (user == null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			listaUsuarios.add(user);
		}
		return new ResponseEntity<List<Usuario>>(listaUsuarios, HttpStatus.OK);
	}

	// GET
	@RequestMapping(value = "/usuario/{idUsuario}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<Usuario> getUsuario(@PathVariable("idUsuario") Long idUsuario) {
		Usuario usuario = usuarioService.findById(idUsuario);
		if (usuario == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
	}

	// DELETE
	@RequestMapping(value = "/usuario/{idUsuario}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public ResponseEntity<?> deleteUsuarioById(@PathVariable("idUsuario") Long idUsuario) {
		Usuario usuario = usuarioService.findById(idUsuario);
		if (usuario == null) {
			return new ResponseEntity("Error No existe el Usuario con el id " + idUsuario, HttpStatus.NOT_FOUND);
		}
		usuarioService.deleteUsuarioById(usuario.getIdUsuario());
		return new ResponseEntity<Usuario>(HttpStatus.OK);
	}

	// CREATE
	@RequestMapping(value = "/usuario", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> createUsuario(@RequestBody Usuario usuario, UriComponentsBuilder uriComponentsBuilder) {
		if (usuarioService.findByUser(usuario.getUsuario()) != null) {
			return new ResponseEntity("Error al Crear el Registro, Usuario ya existente " + usuario.getUsuario(),
					HttpStatus.CONFLICT);
		}
		usuarioService.saveUsuario(usuario);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/api/v1/usuario/{idUsuario}")
				.buildAndExpand(usuario.getIdUsuario()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// UPDATE
	@RequestMapping(value = "/usuario", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseEntity<Usuario> updateUsuario(@RequestBody Usuario usuario,
			UriComponentsBuilder uriComponentsBuilder) {
		if (usuarioService.findByUser(usuario.getUsuario()) != null) {
			return new ResponseEntity("Error al actualizar el registro, Usuario ya existente " + usuario.getUsuario(),
					HttpStatus.CONFLICT);
		}
		usuarioService.updateUsuario(usuario);
		return new ResponseEntity<Usuario>(usuario, HttpStatus.OK);
	}
}
