package com.softdev.core.biometrico.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.List;
import java.util.ArrayList;
import java.lang.Long;
import com.softdev.core.biometrico.service.PermisoNivelAccesoService;
import com.softdev.core.biometrico.eis.bo.Departamento;
import com.softdev.core.biometrico.eis.bo.PermisoNivelAcceso;

@Controller
@RequestMapping("/api/v1")
public class PermisoNivelAccesoController {
	@Autowired
	private PermisoNivelAccesoService permisoNivelAccesoService;

	// GET
	@RequestMapping(value = "/permisoNivelAccesos", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<PermisoNivelAcceso>> getPermisoNivelAccesos(
			@RequestParam(value = "idPermisoNivelAcceso", required = false) Long idPermisoNivelAcceso) {
		List<PermisoNivelAcceso> listaPermisoNivelAccesos = new ArrayList<PermisoNivelAcceso>();
		if (idPermisoNivelAcceso == null) {
			listaPermisoNivelAccesos = permisoNivelAccesoService.findAllPermisoNivelAccesos();
			if (listaPermisoNivelAccesos.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<List<PermisoNivelAcceso>>(listaPermisoNivelAccesos, HttpStatus.OK);
		} else {
			PermisoNivelAcceso permisoNivelAcceso = permisoNivelAccesoService.findById(idPermisoNivelAcceso);
			if (permisoNivelAcceso == null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			listaPermisoNivelAccesos.add(permisoNivelAcceso);
		}
		return new ResponseEntity<List<PermisoNivelAcceso>>(listaPermisoNivelAccesos, HttpStatus.OK);
	}

	// GET
	@RequestMapping(value = "/permisoNivelAcceso/{idPermisoNivelAcceso}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<PermisoNivelAcceso> getPermisoNivelAccesoById(
			@PathVariable("idPermisoNivelAcceso") Long idPermisoNivelAcceso) {
		PermisoNivelAcceso permisoNivelAcceso = permisoNivelAccesoService.findById(idPermisoNivelAcceso);
		if (permisoNivelAcceso == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<PermisoNivelAcceso>(permisoNivelAcceso, HttpStatus.OK);
	}

	// DELETE
	@RequestMapping(value = "/permisoNivelAcceso/{idPermisoNivelAcceso}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public ResponseEntity<?> deletePermisoNivelAccesoById(
			@PathVariable("idPermisoNivelAcceso") Long idPermisoNivelAcceso) {
		PermisoNivelAcceso permisoNivelAcceso = permisoNivelAccesoService.findById(idPermisoNivelAcceso);
		if (permisoNivelAcceso == null) {
			return new ResponseEntity("Error, No existe el Permiso Nivel de Acceso con el id " + idPermisoNivelAcceso,
					HttpStatus.NOT_FOUND);
		}
		permisoNivelAccesoService.deletePermisoNivelAccesoById(permisoNivelAcceso.getIdPermisoNivelAcceso());
		return new ResponseEntity<PermisoNivelAcceso>(HttpStatus.NO_CONTENT);
	}

	// CREATE
	@RequestMapping(value = "/permisoNivelAcceso", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> createPermisoNivelAcceso(@RequestBody PermisoNivelAcceso permisoNivelAcceso,
			UriComponentsBuilder uriComponentsBuilder) {
		if (permisoNivelAccesoService.findByIdPermiso(permisoNivelAcceso.getPermiso().getIdPermiso()) != null
				&& permisoNivelAccesoService
						.findByIdNivelAcceso(permisoNivelAcceso.getNivelAcceso().getIdNivelAcceso()) != null) {
			return new ResponseEntity(
					"Error al Crear el registro, ya existe un Permiso Nivel Acceso Con esas Descripciones "
							+ permisoNivelAcceso.getNivelAcceso().getDescripcion() + " "
							+ permisoNivelAcceso.getPermiso().getDescripcion(),
					HttpStatus.CONFLICT);
		}
		permisoNivelAccesoService.savePermisoNivelAcceso(permisoNivelAcceso);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/api/v1/permisoNivelAcceso/{idPermisoNivelAcceso}")
				.buildAndExpand(permisoNivelAcceso.getIdPermisoNivelAcceso()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// UPDATE
	@RequestMapping(value = "/permisoNivelAcceso", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseEntity<PermisoNivelAcceso> updatePermisoNivelAcceso(
			@RequestBody PermisoNivelAcceso permisoNivelAcceso, UriComponentsBuilder uriComponentsBuilder) {
		if (permisoNivelAccesoService.findByIdPermiso(permisoNivelAcceso.getPermiso().getIdPermiso()) != null
				&& permisoNivelAccesoService
						.findByIdNivelAcceso(permisoNivelAcceso.getNivelAcceso().getIdNivelAcceso()) != null) {
			return new ResponseEntity(
					"Error al Modificar el registro, ya existe un Permiso Nivel Acceso Con esas Descripciones "
							+ permisoNivelAcceso.getNivelAcceso().getDescripcion() + " "
							+ permisoNivelAcceso.getPermiso().getDescripcion(),
					HttpStatus.CONFLICT);
		}
		permisoNivelAccesoService.updatePermisoNivelAcceso(permisoNivelAcceso);
		return new ResponseEntity<PermisoNivelAcceso>(permisoNivelAcceso, HttpStatus.OK);
	}
}
