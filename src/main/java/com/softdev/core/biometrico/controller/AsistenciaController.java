package com.softdev.core.biometrico.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.List;
import java.util.ArrayList;
import java.lang.Long;
import java.sql.Date;

import com.softdev.core.biometrico.service.AsistenciaService;
import com.softdev.core.biometrico.eis.bo.Asistencia;
import com.softdev.core.biometrico.eis.bo.Departamento;
import com.softdev.core.biometrico.eis.bo.PermisoNivelAcceso;

@Controller
@RequestMapping("/api/v1")
public class AsistenciaController {
	@Autowired
	private AsistenciaService asistenciaService;

	// GET
	@RequestMapping(value = "/asistencias", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Asistencia>> getAsistencias(
			@RequestParam(value = "idAsistencia", required = false) Long idAsistencia) {
		List<Asistencia> listaAsistencias = new ArrayList<Asistencia>();
		if (idAsistencia == null) {
			listaAsistencias = asistenciaService.findAllAsistencias();
			if (listaAsistencias.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<List<Asistencia>>(listaAsistencias, HttpStatus.OK);
		} else {
			Asistencia asistencia = asistenciaService.findById(idAsistencia);
			if (asistencia == null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			listaAsistencias.add(asistencia);
		}
		return new ResponseEntity<List<Asistencia>>(listaAsistencias, HttpStatus.OK);
	}

	// GET
	@RequestMapping(value = "/asistencia/{idAsistencia}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<Asistencia> getAsistenciaById(@PathVariable("idAsistencia") Long idAsistencia) {
		Asistencia asistencia = asistenciaService.findById(idAsistencia);
		if (asistencia == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Asistencia>(asistencia, HttpStatus.OK);
	}

	// DELETE
	@RequestMapping(value = "/asistencia/{idAsistencia}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public ResponseEntity<?> deleteAsistencia(@PathVariable("idAsistencia") Long idAsistencia) {
		Asistencia asistencia = asistenciaService.findById(idAsistencia);
		if (asistencia == null) {
			return new ResponseEntity("Error, No existe la Asistencia con el Id " + idAsistencia, HttpStatus.NOT_FOUND);
		}
		asistenciaService.deleteAsistenciaById(asistencia.getIdAsistencia());
		return new ResponseEntity<Asistencia>(HttpStatus.NO_CONTENT);
	}

	// CREATE
	@RequestMapping(value = "/asistencia", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> createAsistencia(@RequestBody Asistencia asistencia,
			UriComponentsBuilder uriComponentsBuilder) {
		/*
		 * if(asistenciaService.findByName(asistencia.getNombre()) != null) { return new
		 * ResponseEntity("Error al Crear el registro, ya existe un departamento con el nombre "
		 * + departamento.getNombreDepartamento(),HttpStatus.CONFLICT); }
		 */
		asistenciaService.saveAsistencia(asistencia);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/api/v1/asistencia/{idAsistencia}")
				.buildAndExpand(asistencia.getIdAsistencia()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// UPDATE
	@RequestMapping(value = "/asistencia", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseEntity<Asistencia> updateAsistencia(@RequestBody Asistencia asistencia,
			UriComponentsBuilder uriComponentsBuilder) {

		/*
		 * if(asistenciaService.findById(asistencia.getIdAsistencia()) != null) { return
		 * new
		 * ResponseEntity("Error al actualizar el registro, una asistencia existe con ese Id "
		 * + asistencia.getIdAsistencia(),HttpStatus.CONFLICT); }
		 */
		System.out.println(asistencia.getFecha());
		asistenciaService.updateAsistencia(asistencia);
		System.out.println(asistencia.getFecha());
		return new ResponseEntity<Asistencia>(asistencia, HttpStatus.OK);
	}
}
