package com.softdev.core.biometrico.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;
import java.util.List;
import java.util.ArrayList;
import java.lang.Long;
import com.softdev.core.biometrico.service.PersonaService;
import com.softdev.core.biometrico.eis.bo.NivelAcceso;
import com.softdev.core.biometrico.eis.bo.Persona;

@Controller
@RequestMapping("/api/v1")
public class PersonaController {
	@Autowired
	private PersonaService personaService;

	// GET
	@RequestMapping(value = "/personas", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Persona>> getPersonas(
			@RequestParam(value = "idPersona", required = false) Long idPersona) {
		List<Persona> listaPersonas = new ArrayList<Persona>();
		if (idPersona == null) {
			listaPersonas = personaService.findAllPersonas();
			if (listaPersonas.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<List<Persona>>(listaPersonas, HttpStatus.OK);
		} else {
			Persona persona = personaService.findById(idPersona);
			if (persona == null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			listaPersonas.add(persona);
		}
		return new ResponseEntity<List<Persona>>(listaPersonas, HttpStatus.OK);
	}

	// GET
	@RequestMapping(value = "/persona/{idPersona}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<Persona> getPersonaById(@PathVariable("idPersona") Long idPersona) {
		Persona persona = personaService.findById(idPersona);
		if (persona == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Persona>(persona, HttpStatus.OK);
	}

	// DELETE
	@RequestMapping(value = "/persona/{idPersona}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public ResponseEntity<?> deletePersonaById(@PathVariable("idPersona") Long idPersona) {
		Persona persona = personaService.findById(idPersona);
		if (persona == null) {
			return new ResponseEntity("Error, No existe la Persona con el id " + idPersona, HttpStatus.NOT_FOUND);
		}
		personaService.deletePersonaById(persona.getIdPersona());
		return new ResponseEntity<Persona>(HttpStatus.NO_CONTENT);
	}

	// CREATE
	@RequestMapping(value = "/persona", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> createPersona(@RequestBody Persona persona, UriComponentsBuilder uriComponentsBuilder) {
		if (personaService.findByCui(persona.getCui()) != null) {
			return new ResponseEntity(
					"Error al Crear el registro, ya existe una Persona con ese CUI " + persona.getCui(),
					HttpStatus.CONFLICT);
		}
		personaService.savePersona(persona);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/api/v1/persona/{idPersona}")
				.buildAndExpand(persona.getIdPersona()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// UPDATE
	@RequestMapping(value = "/persona", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseEntity<Persona> updatePersona(@RequestBody Persona persona,
			UriComponentsBuilder uriComponentsBuilder) {
		if (personaService.findByCui(persona.getCui()) != null) {
			return new ResponseEntity(
					"Error al Actualizar el registro, una persona ya existe con el CUI " + persona.getCui(),
					HttpStatus.CONFLICT);
		}
		personaService.updatePersona(persona);
		return new ResponseEntity<Persona>(persona, HttpStatus.OK);
	}
}
