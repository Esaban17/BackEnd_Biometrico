package com.softdev.core.biometrico.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.ArrayList;
import com.softdev.core.biometrico.service.HorarioPersonaService;
import com.softdev.core.biometrico.eis.bo.HorarioPersona;

@Controller
@RequestMapping("/api/v1")
public class HorarioPersonaController {
	@Autowired
	private HorarioPersonaService horarioPersonaService;

	// GET
	@RequestMapping(value = "/horarioPersonas", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<HorarioPersona>> getHorarioPersonas(
			@RequestParam(value = "idHorarioPersona", required = false) Long idHorarioPersona) {
		List<HorarioPersona> listaHorarioPersonas = new ArrayList<HorarioPersona>();
		if (idHorarioPersona == null) {
			listaHorarioPersonas = horarioPersonaService.findAllHorarioPersonas();
			if (listaHorarioPersonas.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<List<HorarioPersona>>(listaHorarioPersonas, HttpStatus.OK);
		} else {
			HorarioPersona horarioPersona = horarioPersonaService.findById(idHorarioPersona);
			if (horarioPersona == null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			listaHorarioPersonas.add(horarioPersona);
		}
		return new ResponseEntity<List<HorarioPersona>>(listaHorarioPersonas, HttpStatus.OK);
	}

	// GET
	@RequestMapping(value = "/horarioPersona/{idHorarioPersona}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<HorarioPersona> getHorarioPersonaById(
			@PathVariable("idHorarioPersona") Long idHorarioPersona) {
		HorarioPersona horarioPersona = horarioPersonaService.findById(idHorarioPersona);
		if (horarioPersona == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<HorarioPersona>(horarioPersona, HttpStatus.OK);
	}

	// DELETE
	@RequestMapping(value = "/horarioPersona/{idHorarioPersona}", method = RequestMethod.DELETE, headers = "Accep=application/json")
	public ResponseEntity<?> deleteHorarioPersonaById(@PathVariable("idHorarioPersona") Long idHorarioPersona) {
		HorarioPersona horarioPersona = horarioPersonaService.findById(idHorarioPersona);
		if (horarioPersona == null) {
			return new ResponseEntity("Error, No existe Ningun Horario Con el id " + idHorarioPersona,
					HttpStatus.NOT_FOUND);
		}
		horarioPersonaService.deleteHorarioPersonaById(horarioPersona.getIdHorarioPersona());
		return new ResponseEntity<HorarioPersona>(HttpStatus.NO_CONTENT);
	}

	// CREATE
	@RequestMapping(value = "/horarioPersona", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> createHorarioPersona(@RequestBody HorarioPersona horarioPersona,
			UriComponentsBuilder uriComponentsBuilder) {
		horarioPersonaService.saveHorarioPersona(horarioPersona);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/api/v1/departamento/{idDepartamento}")
				.buildAndExpand(horarioPersona.getIdHorarioPersona()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// UPDATE
	@RequestMapping(value = "/horarioPersona", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseEntity<HorarioPersona> updateHorarioPersona(@RequestBody HorarioPersona horarioPersona,
			UriComponentsBuilder uriComponentsBuilder) {
		horarioPersonaService.updateHorarioPersona(horarioPersona);
		return new ResponseEntity<HorarioPersona>(horarioPersona, HttpStatus.OK);
	}
}
