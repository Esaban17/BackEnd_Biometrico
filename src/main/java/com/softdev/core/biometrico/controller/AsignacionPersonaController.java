package com.softdev.core.biometrico.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.RequestEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import com.softdev.core.biometrico.service.AsignacionPersonaService;
import com.softdev.core.biometrico.eis.bo.AsignacionPersona;

@Controller
@RequestMapping("/api/v1")
public class AsignacionPersonaController {
	@Autowired
	private AsignacionPersonaService asignacionPersonaService;

	// GET
	@RequestMapping(value = "/asignacionPersonas", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<AsignacionPersona>> getAsignacionPersonas(
			@RequestParam(value = "idAsignacion", required = false) Long idAsignacion) {
		List<AsignacionPersona> listaAsignacionPersonas = new ArrayList<AsignacionPersona>();
		if (idAsignacion == null) {
			listaAsignacionPersonas = asignacionPersonaService.findAllAsignacionPersonas();
			if (listaAsignacionPersonas.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<List<AsignacionPersona>>(listaAsignacionPersonas, HttpStatus.OK);
		} else {
			AsignacionPersona asignacionPersona = asignacionPersonaService.findById(idAsignacion);
			if (asignacionPersona == null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			listaAsignacionPersonas.add(asignacionPersona);
		}
		return new ResponseEntity<List<AsignacionPersona>>(listaAsignacionPersonas, HttpStatus.OK);
	}

	// GET
	@RequestMapping(value = "/asignacionPersona/{idAsignacion}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<AsignacionPersona> getAsignacionPersonaById(@PathVariable("idAsignacion") Long idAsignacion) {
		AsignacionPersona asignacionPersona = asignacionPersonaService.findById(idAsignacion);
		if (asignacionPersona == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<AsignacionPersona>(asignacionPersona, HttpStatus.OK);
	}

	// DELETE
	@RequestMapping(value = "/asignacionPersona/{idAsignacion}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public ResponseEntity<?> deleteAsignacionPersonaById(@PathVariable("idAsignacion") Long idAsignacion) {
		AsignacionPersona asignacionPersona = asignacionPersonaService.findById(idAsignacion);
		if (asignacionPersona == null) {
			return new ResponseEntity("Error no existen ninguna asignacion de persona con el id " + idAsignacion,
					HttpStatus.NOT_FOUND);
		}
		asignacionPersonaService.deleteAsignacionPersonaById(idAsignacion);
		return new ResponseEntity<AsignacionPersona>(HttpStatus.NO_CONTENT);
	}

	// CREATE
	@RequestMapping(value = "/asignacionPersona", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> createAsignacionPersona(@RequestBody AsignacionPersona asignacionPersona,
			UriComponentsBuilder uriComponentsBuilder) {
		if (asignacionPersonaService.findByIdPersona(asignacionPersona.getPersona().getIdPersona()) != null
				&& asignacionPersonaService
						.findByIdDepartamento(asignacionPersona.getDepartamento().getIdDepartamento()) != null
				&& asignacionPersonaService.findByIdRol(asignacionPersona.getRol().getIdRol()) != null
				&& asignacionPersonaService
						.findByIdNivelAcceso(asignacionPersona.getNivelAcceso().getIdNivelAcceso()) != null) {
			return new ResponseEntity(
					"Error al Crear el registro, ya existe una Asignacion Persona con esas descripciones "
							+ asignacionPersona.getPersona().getIdPersona() + " "
							+ asignacionPersona.getDepartamento().getIdDepartamento() + " "
							+ asignacionPersona.getRol().getIdRol() + " "
							+ asignacionPersona.getNivelAcceso().getIdNivelAcceso(),
					HttpStatus.CONFLICT);
		}
		asignacionPersonaService.saveAsignacionPersona(asignacionPersona);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/api/v1/asignacionPersona/{idAsignacion}")
				.buildAndExpand(asignacionPersona.getIdAsignacion()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// UPDATE
	@RequestMapping(value = "/asignacionPersona", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseEntity<AsignacionPersona> updateAsignacionPersona(@RequestBody AsignacionPersona asignacionPersona,
			UriComponentsBuilder uirComponentsBuilder) {
		if (asignacionPersonaService.findByIdPersona(asignacionPersona.getPersona().getIdPersona()) != null
				&& asignacionPersonaService
						.findByIdDepartamento(asignacionPersona.getDepartamento().getIdDepartamento()) != null
				&& asignacionPersonaService.findByIdRol(asignacionPersona.getRol().getIdRol()) != null
				&& asignacionPersonaService
						.findByIdNivelAcceso(asignacionPersona.getNivelAcceso().getIdNivelAcceso()) != null) {
			return new ResponseEntity(
					"Error al Modificar el registro, ya existe una Asignacion Persona con esas descripciones "
							+ asignacionPersona.getPersona().getIdPersona() + " "
							+ asignacionPersona.getDepartamento().getIdDepartamento() + " "
							+ asignacionPersona.getRol().getIdRol() + " "
							+ asignacionPersona.getNivelAcceso().getIdNivelAcceso(),
					HttpStatus.CONFLICT);
		}
		asignacionPersonaService.updateAsignacionPersona(asignacionPersona);
		return new ResponseEntity<AsignacionPersona>(asignacionPersona, HttpStatus.OK);
	}
}
