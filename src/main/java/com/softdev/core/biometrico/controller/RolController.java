package com.softdev.core.biometrico.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.RequestEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.List;
import java.util.ArrayList;
import com.softdev.core.biometrico.service.RolService;
import com.softdev.core.biometrico.eis.bo.Departamento;
import com.softdev.core.biometrico.eis.bo.Rol;

@Controller
@RequestMapping("/api/v1")
public class RolController {
	@Autowired
	private RolService rolService;

	// GET
	@RequestMapping(value = "/roles", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<List<Rol>> getRoles(
			@RequestParam(value = "descripcion", required = false) String descripcion) {
		List<Rol> listaRoles = new ArrayList<Rol>();
		if (descripcion == null) {
			listaRoles = rolService.findAllRoles();
			if (listaRoles.isEmpty()) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			return new ResponseEntity<List<Rol>>(listaRoles, HttpStatus.OK);
		} else {
			Rol rol = rolService.findByDescription(descripcion);
			if (rol == null) {
				return new ResponseEntity(HttpStatus.NO_CONTENT);
			}
			listaRoles.add(rol);
		}
		return new ResponseEntity<List<Rol>>(listaRoles, HttpStatus.OK);
	}

	// GET
	@RequestMapping(value = "/rol/{idRol}", method = RequestMethod.GET, headers = "Accept=application/json")
	public ResponseEntity<Rol> getRolById(@PathVariable("idRol") Long idRol) {
		Rol rol = rolService.findById(idRol);
		if (rol == null) {
			return new ResponseEntity(HttpStatus.NO_CONTENT);
		}
		return new ResponseEntity<Rol>(rol, HttpStatus.OK);
	}

	// DELETE
	@RequestMapping(value = "/rol/{idRol}", method = RequestMethod.DELETE, headers = "Accept=application/json")
	public ResponseEntity<?> deleteRol(@PathVariable("idRol") Long idRol) {
		Rol rol = rolService.findById(idRol);
		if (rol == null) {
			return new ResponseEntity("Error, No existe el rol con el id " + idRol, HttpStatus.NOT_FOUND);
		}
		rolService.deleteRolById(rol.getIdRol());
		return new ResponseEntity<Rol>(HttpStatus.NO_CONTENT);
	}

	// CREATE
	@RequestMapping(value = "/rol", method = RequestMethod.POST, headers = "Accept=application/json")
	public ResponseEntity<?> createRol(@RequestBody Rol rol, UriComponentsBuilder uriComponentsBuilder) {
		if (rolService.findByDescription(rol.getDescripcion()) != null) {
			return new ResponseEntity(
					"Error al Crear el registro, ya existe un rol con la descripcion " + rol.getDescripcion(),
					HttpStatus.CONFLICT);
		}
		rolService.saveRol(rol);
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponentsBuilder.path("/api/v1/rol/{idRol}").buildAndExpand(rol.getIdRol()).toUri());
		return new ResponseEntity<String>(headers, HttpStatus.CREATED);
	}

	// UPDATE
	@RequestMapping(value = "/rol", method = RequestMethod.PUT, headers = "Accept=application/json")
	public ResponseEntity<Rol> updateRol(@RequestBody Rol rol, UriComponentsBuilder uriComponentsBuilder) {
		if (rolService.findByDescription(rol.getDescripcion()) != null) {
			return new ResponseEntity(
					"Error al actualizar el registro, ya existe un Rol con la descripcion " + rol.getDescripcion(),
					HttpStatus.CONFLICT);
		}
		rolService.updateRol(rol);
		return new ResponseEntity<Rol>(rol, HttpStatus.OK);
	}
}
